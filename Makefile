# SPDX-FileCopyrightText: Adriel Dumas--Jondeau
#
# SPDX-License-Identifier: GPL-3.0-or-later

SCHEME ?= raco
ELM ?= elm
ELM-TEST ?= elm-test

.PHONY: clean bin/forms

bin: bin/forms bin/index.html bin/saisie-server

bin/forms:
	mkdir --parents ./bin/
	rm --recursive --force ./bin/forms/
	mkdir --parents ./bin/forms/
	cp --recursive --force ./doc/examples/* ./bin/forms

bin/index.html:
	cd ./client/ && \
	$(ELM) make --optimize ./src/Main.elm --output=../bin/index.html

bin/saisie-server:
	$(SCHEME) pkg install --batch --deps search-auto yaml threading
	$(SCHEME) make ./server/main.rkt
	$(SCHEME) exe -o ./bin/saisie-server ./server/main.rkt

run: bin
	cd ./bin/ && \
	./saisie-server

dev: bin/forms bin/saisie-server
	cd ./client && $(ELM) make --debug ./src/Main.elm --output=../bin/index.html; \
	inotifywait -m -e modify -r ./src | \
	while read; do $(ELM) make --debug ./src/Main.elm --output=../bin/index.html; done &
	cd ./bin && ./saisie-server

testing:
	cd ./client && $(ELM-TEST) --watch

win: bin/forms bin/index.html
	$(SCHEME) pkg install --batch --deps search-auto raco-cross
	$(SCHEME) cross --target x86_64-win32 pkg install --batch --deps search-auto yaml threading web-server || true
	$(SCHEME) cross --target x86_64-win32 make server/main.rkt
	$(SCHEME) cross --target x86_64-win32 exe -o bin/saisie-server.exe server/main.rkt
	$(SCHEME) cross --target x86_64-win32 dist win/ bin/saisie-server.exe
	cp --recursive --force bin/index.html bin/forms win/


clean:
	rm --recursive --force ./bin/ ./win/
