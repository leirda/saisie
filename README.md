<!--
SPDX-FileCopyrightText: Adriel Dumas--Jondeau <adrieldj@orange.fr>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Saisie

Une application de saisie de donnée utilisable depuis le navigateur (en mode client/serveur).

En lançant le serveur, on peut spécifier le chemin où sauvegarder les données utilisatrice avec l’option `--output`.
Le serveur écrira en `json` dans le fichier spécifié.

# Compilation

## Client

Pour compiler le code client, il est nécessaire de télécharger `elm`.
Suivre les instructions dans le guide d’installation
https://guide.elm-lang.org/install/elm.html

Puis, exécuter la commande suivante depuis la racine du dépôt.
```
elm make --optimize client/src/Main.elm
```

l’`index.html` ainsi généré peut être servie par n’importe quel serveur statique.
Il ne peut cependant être ouvert dans le navigateur car il ne gère que les URLs
(Les URI `http[s]://`, a contrario d’une URI `file://` qui n’est pas supportée).

## Serveur avec Guix

Vous pouvez utiliser le Makefile fournis avec le logiciel GNU Make.

- Taper `make bin` dans le répertoire `server/`.
  Ceci devrait générer un dossier `bin/` avec toutes les dépendances requises.

- Taper `make dev` pour avoir un environnement qui recompile automatiquement le code côté client.

## Compilation croisée pour Windows avec Guix

```sh
guix time-machine --channels=./channels.scm \
     -- shell --manifest=./manifest.scm \
     --container --network --emulate-fhs \
     -- make win
```
