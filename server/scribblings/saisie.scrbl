; SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau <adrieldj@orangef.r>
;
; SPDX-License-Identifier: GPL-3.0-or-later

#lang scribble/manual
@require[@for-label[saisie
                    racket/base]]

@title{saisie}
@author{Adriel}

@defmodule[saisie]

Ce serveur fournit les formulaires et les données pour le logiciel « Saisie ».
