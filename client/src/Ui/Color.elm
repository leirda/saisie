module Ui.Color exposing (color, element)

import Color exposing (Color, rgb255, rgba)
import Element


color :
    { back :
        { alpha : Color
        , main : Color
        , navBar : Color
        , popup : Color
        }
    , input :
        { confirm : Color
        , button : Color
        , delete : Color
        , text : Color
        , hover :
            { confirm : Color
            , button : Color
            , delete : Color
            , text : Color
            }
        }
    , text :
        { error : Color
        , hint : Color
        , normal : Color
        , success : Color
        }
    }
color =
    { back =
        { alpha = rgba 1 1 1 0
        , main = rgb255 0x26 0x12 0x31
        , navBar = rgb255 0x1B 0x0D 0x22
        , popup = rgba 0 0 0 0.75
        }
    , input =
        { confirm = rgb255 0x30 0x67 0x63
        , delete = rgb255 0x73 0x16 0x42
        , button = rgb255 0x43 0x26 0x53
        , text = rgb255 0x35 0x1F 0x42
        , hover =
            { confirm = rgb255 0x1F 0x9C 0x6F
            , button = rgb255 0x56 0x24 0x6D
            , delete = rgb255 0xA8 0x21 0x49
            , text = rgb255 0x5E 0x1E 0x86
            }
        }
    , text =
        { error = rgb255 0xCB 0x1F 0x53
        , hint = rgb255 0xA5 0x7B 0xB4
        , normal = rgb255 0xFB 0xE5 0xFF
        , success = rgb255 0x1F 0x9C 0x6F
        }
    }


element : Color -> Element.Color
element =
    Color.toRgba >> Element.fromRgb
