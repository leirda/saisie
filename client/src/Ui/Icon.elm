module Ui.Icon
    exposing
        ( add
        , cancel
        , checkbox
        , confirm
        , delete
        , duplicate
        , fold
        , foldAll
        , foldEmpty
        , home
        , option
        , search
        , unfoldAll
        , unfoldEmpty
        )

import TypedSvg exposing (circle, g, line, polygon, polyline, rect, svg)
import TypedSvg.Attributes
    exposing
        ( fill
        , points
        , stroke
        , strokeLinecap
        , strokeLinejoin
        , transform
        , viewBox
        )
import TypedSvg.Attributes.InPx exposing (..)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Types
    exposing
        ( Paint(..)
        , StrokeLinecap(..)
        , StrokeLinejoin(..)
        , Transform(..)
        )
import Ui.Color exposing (color)


checkbox : Bool -> Svg msg
checkbox checked =
    [ rect
        [ x 0
        , y 0
        , width 1
        , height 1
        , rx 0.15
        , fill
            (if checked then
                Paint color.text.hint

             else
                Paint color.input.button
            )
        ]
        []
    ]
        |> frame


option : Bool -> Svg msg
option checked =
    [ circle
        [ cx 0.5
        , cy 0.5
        , r 0.5
        , fill
            (if checked then
                Paint color.text.hint

             else
                Paint color.input.button
            )
        ]
        []
    ]
        |> frame


add : Svg msg
add =
    [ regularLine ( 0.5, 0.1 ) ( 0.5, 0.9 )
    , regularLine ( 0.1, 0.5 ) ( 0.9, 0.5 )
    ]
        |> frame


search : Svg msg
search =
    [ circle
        [ cx 0.37
        , cy 0.37
        , r 0.32
        , strokeWidth 0.1
        , fill PaintNone
        , stroke (Paint color.text.hint)
        ]
        []
    , line
        [ x1 0.6
        , y1 0.6
        , x2 0.85
        , y2 0.85
        , strokeWidth 0.1
        , strokeLinecap StrokeLinecapRound
        , stroke (Paint color.text.hint)
        ]
        []
    ]
        |> frame


tagProgress : Svg msg
tagProgress =
    [ regularRhombus
    ]
        |> frame


tagArchive : Svg msg
tagArchive =
    [ regularRhombus
    , regularPolygon [ ( 0.34, 0.5 ), ( 0.5, 0.34 ), ( 0.66, 0.5 ), ( 0.5, 0.66 ) ]
    ]
        |> frame


tagTransfer : Svg msg
tagTransfer =
    [ regularPolygon [ ( 0.8, 0.34 ), ( 0.5, 0.34 ), ( 0.34, 0.5 ), ( 0.5, 0.66), ( 0.8, 0.66) ]
    , polygon 
        [ points [ ( 0.47, 0.0 ), ( 0.5, 0.0 ), ( 0.6, 0.1 ), ( 0.6, 0.22 ), ( 0.5, 0.12 ) ]
        , fill (Paint color.text.hint)
        ]
        []
    , polygon 
        [ points [ ( 0.47, 1.0 ), ( 0.5, 1.0 ), ( 0.6, 0.9 ), ( 0.6, 0.78 ), ( 0.5, 0.88 ) ]
        , fill (Paint color.text.hint)
        ]
        []
    , polyline
        [ points [ ( 0.5, 0.05), ( 0.05, 0.5 ), ( 0.5, 0.95 ) ]
        , fill PaintNone
        , strokeWidth 0.1
        , strokeLinejoin StrokeLinejoinRound
        , stroke (Paint color.text.hint)
        ]
        []
    , rect
        [ x 0.7
        , y 0.29
        , rx 0.05
        , width 0.3
        , height 0.42
        , fill (Paint color.text.hint)
        ]
        []
    ]
        |> frame


tagProgress2 : Svg msg
tagProgress2 =
    [ regularPolyline [ ( 0.55, 0.05 ), ( 0.15, 0.05 ), ( 0.15, 0.95 ), ( 0.85, 0.95 ), ( 0.85, 0.4 ) ]
    , polygon
        [ points [ ( 0.55, 0.05 ), ( 0.85, 0.4 ), ( 0.55, 0.4 ) ]
        , fill (Paint color.text.hint)
        , strokeWidth 0.1
        , strokeLinejoin StrokeLinejoinRound
        , stroke (Paint color.text.hint)
        ]
        []
    ]
        |> frame


tagTransfer2 : Svg msg
tagTransfer2 =
    [ regularPolyline [ ( 0.25, 0.95 ), ( 0.95, 0.95 ), ( 0.95, 0.4 ), ( 0.65, 0.05 ), ( 0.25, 0.05 ) ]
    , regularPolyline [ ( 0.3, 0.5 ), ( 0.55, 0.5 ) ]
    , polygon
        [ points [ ( 0.28, 0.25 ), ( 0.25, 0.25 ), ( 0.05, 0.5 ), ( 0.25, 0.75 ), ( 0.28, 0.75 ) ]
        , fill (Paint color.text.hint)
        , strokeWidth 0.1
        , strokeLinejoin StrokeLinejoinRound
        , stroke (Paint color.text.hint)
        ]
        []
    ]
        |> frame


flagArchive2 : Svg msg
flagArchive2 =
    [ rect
        [ x 0.05
        , y 0.15
        , rx 0.02
        , width 0.9
        , height 0.2
        , fill PaintNone
        , strokeWidth 0.1
        , strokeLinejoin StrokeLinejoinRound
        , stroke (Paint color.text.hint)
        ]
        []
    , polyline
        [ points [ ( 0.15, 0.5 ), ( 0.15, 0.95 ), ( 0.85, 0.95 ), ( 0.85, 0.5 ) ]
        , fill PaintNone
        , strokeWidth 0.1
        , strokeLinejoin StrokeLinejoinRound
        , stroke (Paint color.text.hint)
        ]
        []
    , regularPolyline [ ( 0.42, 0.55 ), ( 0.58, 0.55 ) ]
    ]
        |> frame    


filter : Svg msg
filter =
    [ polygon
        [ points [ ( 0.1, 0.08 ), ( 0.9, 0.08 ), ( 0.5, 0.5 ) ]
        , fill (Paint color.text.hint)
        , strokeWidth 0.1
        , strokeLinejoin StrokeLinejoinRound
        , stroke (Paint color.text.hint)
        ]
        []
    , polygon
        [ points [ ( 0.43, 0.48 ), ( 0.57, 0.48 ), ( 0.57, 0.95 ), ( 0.43, 0.87 ) ]
        , fill (Paint color.text.hint)
        , strokeWidth 0.05
        , strokeLinejoin StrokeLinejoinRound
        , stroke (Paint color.text.hint)
        ]
        []
    ]
        |> frame


flagOff : Svg msg
flagOff =
    [ polygon
        [ points [ ( 0.08, 0.05 ), ( 0.08, 0.12 ), ( 0.92, 0.12 ), ( 0.7, 0.36 ), ( 0.92, 0.61 ), ( 0.08, 0.61 ), ( 0.08, 1.0 ) ] 
        , fill PaintNone
        , strokeWidth 0.1
        , strokeLinejoin StrokeLinejoinRound
        , strokeLinecap StrokeLinecapRound
        , stroke (Paint color.text.hint)
        ]
        []
    ]
        |> frame


flagOn : Svg msg
flagOn =
    [ polygon
        [ points [ ( 0.3, 0.12 ), ( 0.92, 0.12 ), ( 0.7, 0.36 ), ( 0.92, 0.61 ), ( 0.3, 0.61 ) ] 
        , fill (Paint color.text.hint)
        , strokeWidth 0.1
        , strokeLinejoin StrokeLinejoinRound
        , strokeLinecap StrokeLinecapRound
        , stroke (Paint color.text.hint)
        ]
        []
    , regularPolyline [ ( 0.08, 0.05), ( 0.08, 1.0 ) ]
    ]
        |> frame


home : Svg msg
home =
    [ polygon
        [ points [ (0.15, 1.0), (0.15, 0.5), (0.0, 0.5), (0.5, 0.0), (1.0, 0.5), (0.85, 0.5), (0.85, 1.0), (0.6, 1.0), (0.6, 0.7), (0.4, 0.7), (0.4, 1.0) ]
        , fill (Paint color.text.hint)
        ]
        []
    ]
        |> frame


delete : Svg msg
delete =
    [ polyline
        [ points [ ( 0.2, 0.32 ), ( 0.2, 0.9), ( 0.25, 0.95 ), ( 0.75, 0.95 ), ( 0.8, 0.9 ), ( 0.8, 0.32 ) ]
        , strokeWidth 0.1
        , fill PaintNone
        , strokeLinejoin StrokeLinejoinRound
        , stroke (Paint color.text.normal)
        ]
        []
    , regularLine ( 0.07, 0.15 ) ( 0.93, 0.15 )
    , regularLine ( 0.6, 0.052 ) ( 0.4, 0.052 )
    ]
        |> frame


refuse : Svg msg
refuse =
    [ regularLine ( 0.8, 0.8 ) ( 0.2, 0.2 )
    , regularLine ( 0.2, 0.8 ) ( 0.8, 0.2 )
    ]
        |> frame


duplicate : Svg msg
duplicate =
    [ regularPolyline [ ( 0.1, 0.3 ), ( 0.1, 0.9 ), ( 0.7, 0.9 ) ]
    , rect
        [ x 0.3
        , y 0.1
        , rx 0.02
        , width 0.6
        , height 0.6
        , fill (Paint color.text.hint)
        , strokeWidth 0.1
        , strokeLinejoin StrokeLinejoinRound
        , stroke (Paint color.text.hint)
        ]
        []
    ]
        |> frame


confirm : Svg msg
confirm =
    [ circle
        [ cx 0.5
        , cy 0.5
        , r 0.4
        , strokeWidth 0.1
        , fill PaintNone
        , stroke (Paint color.text.normal)
        ]
        []
    ]
        |> frame


cancel : Svg msg
cancel =
    [ regularLine ( 0.8, 0.8 ) ( 0.2, 0.2 )
    , regularLine ( 0.2, 0.8 ) ( 0.8, 0.2 )
    ]
        |> frame


foldReverse : Svg msg
foldReverse =
    [ regularPolyline [ ( 0.12, 0.15 ), ( 0.16, 0.15 ), ( 0.61, 0.5 ), ( 0.16, 0.85 ), ( 0.12, 0.85 ), ( 0.12, 0.15) ]
    , regularPolyline [ ( 0.48, 0.15 ), ( 0.93, 0.5 ), ( 0.48, 0.85 ) ]
    ]
        |> frame


foldEmpty : Svg msg
foldEmpty =
    [ rect
        [ x 0.05
        , y 0.15
        , rx 0.02
        , width 0.9
        , height 0.2
        , fill (Paint color.text.hint)
        , strokeWidth 0.1
        , strokeLinejoin StrokeLinejoinRound
        , stroke (Paint color.text.hint)
        ]
        []
    , regularPolyline [ ( 0.05, 0.6 ), ( 0.95, 0.6 ) ]    
    , regularPolyline [ ( 0.05, 0.85 ), ( 0.95, 0.85 ) ]    
    ]
        |> frame    


unfoldEmpty : Svg msg
unfoldEmpty =
    [ rect
        [ x 0.05
        , y 0.1
        , rx 0.02
        , width 0.9
        , height 0.25
        , fill PaintNone
        , strokeWidth 0.1
        , strokeLinejoin StrokeLinejoinRound
        , stroke (Paint color.text.hint)
        ]
        []
    , regularPolyline [ ( 0.05, 0.6 ), ( 0.95, 0.6 ) ]    
    , regularPolyline [ ( 0.05, 0.85 ), ( 0.95, 0.85 ) ]    
    ]
        |> frame    


fold : Bool -> Svg msg
fold isFolded =
    [ if isFolded then
        regularPolygon
            [ ( 0.1, 0.1 )
            , ( 0.23, 0.1 )
            , ( 0.9, 0.5 )
            , ( 0.23, 0.9 )
            , ( 0.1, 0.9 )
            ]

      else
        regularPolygon
            [ ( 0.1, 0.1 )
            , ( 0.1, 0.23 )
            , ( 0.5, 0.9 )
            , ( 0.9, 0.23 )
            , ( 0.9, 0.1 )
            ]
    ]
        |> frame


foldAll : Svg msg
foldAll =
    foldHelper regularPolygon


unfoldAll : Svg msg
unfoldAll =
    unfoldHelper regularPolygon



-- FOLDS HELPERS


foldHelper : (List ( Float, Float ) -> Svg msg) -> Svg msg
foldHelper fillFold =
    [ fillFold
        [ ( 0.12, 0.15 )
        , ( 0.16, 0.15 )
        , ( 0.61, 0.5 )
        , ( 0.16, 0.85 )
        , ( 0.12, 0.85 )
        ]
    , regularPolyline
        [ ( 0.48, 0.15 )
        , ( 0.93, 0.5 )
        , ( 0.48, 0.85 )
        ]
    ]
        |> frame


unfoldHelper : (List ( Float, Float ) -> Svg msg) -> Svg msg
unfoldHelper foldPolygon =
    [ foldPolygon
        [ ( 0.15, 0.12 )
        , ( 0.15, 0.16 )
        , ( 0.5, 0.61 )
        , ( 0.85, 0.16 )
        , ( 0.85, 0.12 )
        ]
    , regularPolyline
        [ ( 0.15, 0.48 )
        , ( 0.5, 0.93 )
        , (  0.85, 0.48 )
        ]
    ]
        |> frame


-- HELPERS


frame : List (Svg msg) -> Svg msg
frame =
    svg [ viewBox 0 0 1 1 ]


regularLine : ( Float, Float ) -> ( Float, Float ) -> Svg msg
regularLine ( xStart, yStart ) ( xEnd, yEnd ) =
    line
        [ x1 xStart
        , y1 yStart
        , x2 xEnd
        , y2 yEnd
        , strokeWidth 0.1
        , strokeLinecap StrokeLinecapRound
        , stroke (Paint color.text.normal)
        ]
        []


regularPolyline : List ( Float, Float ) -> Svg msg
regularPolyline myPoints =
    polyline
        [ points myPoints
        , fill PaintNone
        , strokeWidth 0.1
        , strokeLinecap StrokeLinecapRound
        , strokeLinejoin StrokeLinejoinRound
        , stroke (Paint color.text.hint)
        ]
        []


regularEmptyPolygon : List ( Float, Float ) -> Svg msg
regularEmptyPolygon myPoints =
    polygon
        [ points myPoints
        , fill PaintNone
        , strokeWidth 0.1
        , strokeLinecap StrokeLinecapRound
        , stroke (Paint color.text.hint)
        ]
        []


regularPolygon : List ( Float, Float ) -> Svg msg
regularPolygon myPoints =
    polygon
        [ points myPoints
        , fill (Paint color.text.hint)
        , strokeWidth 0.1
        , strokeLinecap StrokeLinecapRound
        , stroke (Paint color.text.hint)
        ]
        []


regularRhombus : Svg msg
regularRhombus =
    regularPolyline [ ( 0.5, 0.05), ( 0.95, 0.5 ), ( 0.5, 0.95 ), ( 0.05, 0.5 ), ( 0.5, 0.05) ]
