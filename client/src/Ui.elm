module Ui exposing
    ( Ui
    , abortButton
    , applyButton
    , bool
    , createButton
    , createEntryButton
    , createForm
    , date
    , deleteEntry
    , duplicateEntry
    , entryButton
    , error
    , exportButton
    , field
    , focused
    , foldButtons
    , form
    , formButton
    , group
    , importButton
    , info
    , layout
    , link
    , list
    , listItem
    , map
    , newTabLink
    , none
    , oneOf
    , page
    , paragraph
    , quitButton
    , retry
    , rowGroup
    , search
    , searchList
    , select
    , someOf
    , strictOneOf
    , strictSomeOf
    , textInput
    , todo
    )

import Date exposing (Date)
import DatePicker exposing (ChangeEvent(..))
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Html exposing (Html, label)
import Html.Attributes
import Html.Events
import Json.Decode
import List.Extra as List
import Set exposing (Set)
import Simple.Fuzzy as Fuzzy
import String.Normalize as String
import Text exposing (foldAll, foldEmpty)
import Time
import TypedSvg.Core
import Ui.Color exposing (color, element)
import Ui.Icon as Icon


type alias Ui msg =
    Element msg


page :
    { navBar : List (Element msg)
    , content : Element msg
    , footer : List (Element msg)
    }
    -> Element msg
page { content, footer, navBar } =
    column [ width fill, height fill ]
        [ row styleNavBar (List.map (el [ width fill ]) navBar)
        , el styleContent content
        , List.intersperse (el [ width fill ] none) footer
            |> row styleFooter
        ]


form : List (Element msg) -> Element msg
form fields =
    column styleForm fields |> fillCenterPortionX 2


searchList :
    { items : List { button : Element msg, text : String }
    , next : Element msg
    , onChange : String -> msg
    , text : String
    }
    -> Element msg
searchList ({ items, next, onChange, text } as datas) =
    let
        filteredItems : List { button : Element msg, text : String }
        filteredItems =
            List.filter (.text >> String.contains text) items
    in
    column [ width fill, height fill, spacing 3 ]
        [ el
            [ Font.color (element color.text.hint)
            , Font.size 12
            , alignRight
            , paddingXY 3 6
            ]
            (Element.text
                (Text.formsNumber
                    (List.length filteredItems)
                    (List.length items)
                )
            )
        , searchButton datas
        , filteredItems
            |> List.map .button
            |> column
                [ width fill, height fill, scrollbarY, spacing 6, paddingXY 0 6 ]
        ]
        |> fillCenterPortionY 8
        |> fillCenterPortionX 2


search :
    { onChange : String -> msg, next : Element msg, text : String }
    -> Element msg
search searchSettings =
    searchButton searchSettings
        |> fillCenterPortionY 8
        |> fillCenterPortionX 2


error :
    { content : String
    , control : List (Element msg)
    }
    -> Element msg
error { content, control } =
    column
        [ width fill
        , height fill
        , padding 6
        , Border.rounded 6
        , Border.width 3
        , Border.color (element color.input.hover.text)
        ]
        [ el [ Font.bold ] (text Text.error.title)
        , column [ width fill, height fill, paddingXY 12 6, spacing 3 ]
            [ Element.paragraph [ Font.italic ]
                (List.map text Text.error.generic)
            , el
                [ width fill
                , height fill
                , scrollbars
                , padding 6
                , Border.rounded 6
                , Background.color (element color.back.navBar)
                , Font.family [ Font.monospace ]
                , Font.color (element color.text.error)
                ]
                (text content)
            ]
        , control
            |> List.intersperse (el [ width fill ] none)
            |> row [ paddingXY 0 3 ]
        ]
        |> fillCenterPortionX 8
        |> fillCenterPortionY 2


retry : msg -> Element msg
retry onPress =
    Input.button styleButton
        { onPress = Just onPress
        , label = text Text.error.retry
        }


createButton : msg -> Element msg
createButton onPress =
    Input.button styleShrinkButton
        { onPress = Just onPress
        , label = defaultIcon Icon.add
        }


applyButton : { onPress : msg } -> Element msg
applyButton { onPress } =
    Input.button styleConfirmButton
        { label = text Text.saveAndGoToMain
        , onPress = Just onPress
        }


abortButton : { onPress : msg } -> Element msg
abortButton { onPress } =
    Input.button styleDeleteButton
        { label = defaultIcon Icon.home
        , onPress = Just onPress
        }


quitButton : { onPress : msg } -> Element msg
quitButton { onPress } =
    Input.button styleShrinkButton
        { label = defaultIcon Icon.home
        , onPress = Just onPress
        }


exportButton : { onPress : msg } -> Element msg
exportButton { onPress } =
    Input.button (alignRight :: styleShrinkButton)
        { label = text Text.exportButtonText
        , onPress = Just onPress
        }


importButton : { onPress : msg } -> Element msg
importButton { onPress } =
    Input.button (alignRight :: styleShrinkButton)
        { label = text Text.importButtonText
        , onPress = Just onPress
        }



-- FIELDS


type alias Input content msg =
    { content : content
    , onChange : content -> msg
    }


field :
    { isEmpty : content -> Bool
    , parse : content -> Result String result
    }
    ->
        { content : content
        , datas :
            { f
                | description : Maybe String
                , isFolded : Bool
                , name : String
                , isRequired : Bool
            }
        , onFold : msg
        , input : Element msg
        }
    -> Element msg
field { isEmpty, parse } { content, datas, onFold, input } =
    column [ width fill, height fill ]
        [ Input.button styleHiddenButton
            { label = foldDescription datas.isFolded (text datas.name)
            , onPress = Just onFold
            }
        , if datas.isFolded then
            none

          else
            [ datas.description
                |> Maybe.map
                    (text
                        >> List.singleton
                        >> Element.paragraph
                            [ width shrink
                            , alignLeft
                            , paddingXY 20 12
                            , Font.color (element color.text.hint)
                            , Font.size 16
                            ]
                    )
                |> Maybe.withDefault none
            , el
                [ width fill
                , paddingEach { right = 0, top = 6, left = 12, bottom = 6 }
                ]
                input
            , if datas.isRequired && isEmpty content then
                el styleWarning (text Text.isRequired)

              else if not (isEmpty content) then
                case parse content of
                    Ok _ ->
                        none

                    Err err ->
                        el styleWarning (text err)

              else
                none
            ]
                |> column [ width fill ]
        ]


textInput : { hint : Maybe String } -> Input String msg -> Element msg
textInput { hint } { content, onChange } =
    [ { onChange = onChange
      , placeholder = Nothing
      , text = content
      , label = Input.labelHidden ""
      }
        |> Input.text styleFieldButton
    , hint |> Maybe.map (el styleHint << text) |> Maybe.withDefault none
    ]
        |> column [ width fill ]


paragraph : Input String msg -> Element msg
paragraph { content, onChange } =
    Input.multiline (height (minimum 100 shrink) :: styleFieldButton)
        { onChange = onChange
        , placeholder = Nothing
        , spellcheck = True
        , text = content
        , label = Input.labelHidden "line field"
        }


bool : Input Bool msg -> Element msg
bool { content, onChange } =
    Input.checkbox styleSomeOfChoice
        { checked = content
        , icon = defaultIcon << Icon.checkbox
        , label = Input.labelHidden "bool field"
        , onChange = onChange
        }


select :
    Set String
    ->
        Input
            { input : String, bestMatches : List String, keepMatches : Bool }
            msg
    -> Element msg
select choices { content, onChange } =
    let
        matchWith : String -> List String
        matchWith input =
            if String.isEmpty input || Set.member content.input choices then
                []

            else
                Set.toList choices
                    |> Fuzzy.filter identity input
                    |> List.filter ((/=) input)
                    |> List.take 5

        removeSingleMatch : List String -> List String
        removeSingleMatch matches =
            case matches of
                [ _ ] ->
                    []

                _ ->
                    matches

        suggestionButton : String -> Element msg
        suggestionButton match =
            Input.button styleSelectItem
                { label = text match
                , onPress =
                    { content
                        | input = match
                        , bestMatches =
                            removeSingleMatch content.bestMatches
                    }
                        |> onChange
                        |> Just
                }

        manageMouseEvents : List (Attribute msg)
        manageMouseEvents =
            [ Html.Events.stopPropagationOn "mouseup" <|
                Json.Decode.succeed
                    ( onChange { content | keepMatches = False }, True )
            , Html.Events.stopPropagationOn "mousedown" <|
                Json.Decode.succeed
                    ( onChange { content | keepMatches = True }, True )
            ]
                |> List.map htmlAttribute

        suggestionList : Element msg
        suggestionList =
            if List.isEmpty content.bestMatches then
                none

            else
                List.map suggestionButton content.bestMatches
                    |> column (manageMouseEvents ++ styleTooltip)

        onLoseFocus : List (Attribute msg)
        onLoseFocus =
            [ Events.onLoseFocus
                (onChange
                    { content
                        | bestMatches =
                            if content.keepMatches then
                                content.bestMatches

                            else
                                []
                    }
                )
            ]
    in
    Input.text (below suggestionList :: onLoseFocus ++ styleFieldButton)
        { onChange =
            \input ->
                onChange
                    { content | input = input, bestMatches = matchWith input }
        , placeholder = Nothing
        , text = content.input
        , label = Input.labelHidden "select field"
        }


date : Input ( DatePicker.Model, String ) msg -> Element msg
date { content, onChange } =
    DatePicker.input styleFieldButton
        { onChange =
            (\changeEvent ->
                case changeEvent of
                    DateChanged result ->
                        Tuple.mapSecond (\_ -> Date.toIsoString result) content

                    TextChanged input ->
                        Tuple.mapSecond (\_ -> input) content

                    PickerChanged msg ->
                        Tuple.mapFirst
                            (\_ -> DatePicker.update msg (Tuple.first content))
                            content
            )
                >> onChange
        , selected = Result.toMaybe <| Date.fromIsoString <| Tuple.second content
        , text = Tuple.second content
        , label = Input.labelHidden "date field"
        , placeholder =
            text Text.datePlaceholder
                |> Input.placeholder [ Font.color (element color.text.hint) ]
                |> Just
        , settings = styleDatePicker
        , model = Tuple.first content
        }


someOf :
    Set String
    -> Input { r | selected : Set String, other : Maybe String } msg
    -> Element msg
someOf choices { content, onChange } =
    [ strictSomeOf choices
        { content = content.selected
        , onChange =
            (\selected -> { content | selected = selected }) >> onChange
        }
    , Input.checkbox styleSomeOfChoice
        { checked = content.other /= Nothing
        , icon = defaultIcon << Icon.checkbox
        , label =
            Input.labelRight [ width fill, padding 3 ] (text Text.otherField)
        , onChange =
            (\checked ->
                if checked then
                    { content | other = Just "" }

                else
                    { content | other = Nothing }
            )
                >> onChange
        }
    , content.other
        |> Maybe.map
            (\other ->
                textInput
                    { hint = Nothing }
                    { content = other
                    , onChange =
                        (\text -> { content | other = Just text })
                            >> onChange
                    }
                    |> el [ width fill, paddingXY 12 6 ]
            )
        |> Maybe.withDefault none
    ]
        |> column [ width fill ]


strictSomeOf : Set String -> Input (Set String) msg -> Element msg
strictSomeOf choices { content, onChange } =
    List.map
        (\choice ->
            Input.checkbox styleSomeOfChoice
                { checked = Set.member choice content
                , icon = defaultIcon << Icon.checkbox
                , label =
                    Input.labelRight [ width fill, paddingXY 3 2 ]
                        (wrappingText choice)
                , onChange =
                    (\selected ->
                        if selected then
                            Set.insert choice content

                        else
                            Set.remove choice content
                    )
                        >> onChange
                }
        )
        (Set.toList choices |> List.sortBy String.removeDiacritics)
        |> column [ width fill ]


oneOf : Set String -> Input (Maybe String) msg -> Element msg
oneOf choices { content, onChange } =
    [ strictOneOf choices
        { content = Maybe.withDefault "" content
        , onChange =
            (\choice ->
                if String.isEmpty choice then
                    Nothing

                else
                    Just choice
            )
                >> onChange
        }
    , Input.checkbox styleOneOfChoice
        { checked =
            content
                |> Maybe.map (not << (\c -> Set.member c choices))
                |> Maybe.withDefault False
        , icon = defaultIcon << Icon.option
        , label =
            Input.labelRight [ width fill, padding 3 ]
                (text Text.otherField)
        , onChange =
            (\selected ->
                if selected then
                    Just ""

                else
                    Nothing
            )
                >> onChange
        }
    , case content of
        Just choice ->
            if Set.member choice choices then
                none

            else
                textInput
                    { hint = Nothing }
                    { content = choice
                    , onChange = Just >> onChange
                    }
                    |> el [ width fill, paddingXY 12 6 ]

        Nothing ->
            none
    ]
        |> column [ width fill ]


strictOneOf : Set String -> Input String msg -> Element msg
strictOneOf choices { content, onChange } =
    List.map
        (\choice ->
            Input.checkbox styleOneOfChoice
                { checked = content == choice
                , icon = defaultIcon << Icon.option
                , label =
                    Input.labelRight [ width fill, paddingXY 3 2 ]
                        (wrappingText choice)
                , onChange =
                    (\selected ->
                        if selected then
                            choice

                        else
                            ""
                    )
                        >> onChange
                }
        )
        (Set.toList choices |> List.sortBy String.removeDiacritics)
        |> column [ width fill ]


listItem :
    { description : Maybe String
    , view : field -> Element field
    }
    -> { delete : msg, index : Int }
    -> Input { isFolded : Bool, fields : List field } msg
    -> Element msg
listItem { description, view } { delete, index } { content, onChange } =
    [ [ Input.button styleFieldTitle
            { label =
                String.join
                    " #"
                    [ Maybe.withDefault Text.item description
                    , String.fromInt (index + 1)
                    ]
                    |> text
                    |> foldDescription content.isFolded
            , onPress =
                { content | isFolded = not content.isFolded }
                    |> onChange
                    |> Just
            }
      , if content.isFolded then
            none

        else
            Input.button styleDeleteButton
                { label = defaultIcon Icon.delete
                , onPress = Just delete
                }
                |> el [ alignRight ]
      ]
        |> row [ width fill, spacing 6 ]
    , if content.isFolded then
        none

      else
        group view
            { content = content.fields
            , onChange = \fields -> { content | fields = fields } |> onChange
            }
            |> el styleListItem
            |> el
                (paddingEach { right = 0, top = 3, left = 6, bottom = 3 }
                    :: styleForm
                )
    ]
        |> column [ width fill, height shrink ]


list :
    { add :
        { r | items : { isFolded : Bool, fields : fields }, max : Int }
    , duplicate :
        { isFolded : Bool, fields : fields } -> { isFolded : Bool, fields : fields }
    , view :
        { delete : msg, index : Int }
        -> Input { isFolded : Bool, fields : fields } msg
        -> Element msg
    }
    -> Input (List { isFolded : Bool, fields : fields }) msg
    -> Element msg
list { add, duplicate, view } { content, onChange } =
    let
        viewItem : Int -> { isFolded : Bool, fields : fields } -> Element msg
        viewItem index content_ =
            { content = content_
            , onChange =
                \fields -> List.setAt index fields (foldAll content) |> onChange
            }
                |> view
                    { delete = List.removeAt index content |> onChange
                    , index = index
                    }
                |> el [ width fill, height shrink ]

        addItem :
            TypedSvg.Core.Svg msg
            -> { isFolded : Bool, fields : fields }
            -> Element msg
        addItem icon item =
            { label = defaultIcon icon
            , onPress =
                (foldAll content ++ [ duplicate { item | isFolded = False } ])
                    |> onChange
                    |> Just
            }
                |> Input.button styleButton

        foldAll :
            List { isFolded : Bool, fields : fields }
            -> List { isFolded : Bool, fields : fields }
        foldAll =
            List.map (\item -> { item | isFolded = True })
    in
    List.indexedMap viewItem content
        ++ [ if List.length content < add.max then
                [ addItem Icon.add add.items
                , List.find (not << .isFolded) content
                    |> Maybe.map (addItem Icon.duplicate)
                    |> Maybe.withDefault none
                ]
                    |> row [ width fill, spacingXY 6 0 ]

             else
                none
           ]
        |> column [ width fill, spacing 12 ]


group : (field -> Element field) -> Input (List field) msg -> Element msg
group view { content, onChange } =
    List.indexedMap
        (\index ->
            Element.map (\new -> List.setAt index new content |> onChange)
                << view
        )
        content
        |> column styleForm



-- BUILDING BLOCKS


tooltip : Element Never -> Attribute msg
tooltip element =
    el
        [ width fill
        , height fill
        , transparent True
        , mouseOver [ transparent False ]
        , element
            |> el
                [ htmlAttribute
                    (Html.Attributes.style "pointerEvents" "none")
                ]
            |> (Element.map never >> below)
        ]
        none
        |> inFront


searchButton :
    { r
        | onChange : String -> msg
        , next : Element msg
        , text : String
    }
    -> Element msg
searchButton { onChange, next, text } =
    [ Input.search styleSearchInput
        { onChange = onChange
        , text = text
        , placeholder =
            Element.text Text.search
                |> Input.placeholder [ Font.color (element color.text.hint) ]
                |> Just
        , label = Input.labelHidden Text.search
        }
    , next
    ]
        |> row [ width fill, spacingXY 6 0 ]


foldButtons :
    { fields : List field
    , fold : Bool -> field -> field
    , isEmpty : field -> Bool
    , isFolded : field -> Bool
    , onChange : List field -> msg
    }
    -> Element msg
foldButtons { fields, fold, isEmpty, isFolded, onChange } =
    let
        iconButton :
            { label : String
            , icon : TypedSvg.Core.Svg msg
            , onFold : field -> Bool
            }
            -> Element msg
        iconButton { label, icon, onFold } =
            { label = defaultIcon icon
            , onPress =
                Just <| onChange <| List.map (\f -> fold (onFold f) f) fields
            }
                |> Input.button
                    (tooltip (el styleTooltip (text label))
                        :: styleShrinkButton
                    )
    in
    [ if List.all isFolded fields then
        { label = Text.unfoldAll
        , icon = Icon.unfoldAll
        , onFold = always False
        }
            |> iconButton

      else
        { label = Text.foldAll
        , icon = Icon.foldAll
        , onFold = always True
        }
            |> iconButton
    , { label = Text.unfoldEmpty
      , icon = Icon.unfoldEmpty
      , onFold = not << isEmpty
      }
        |> iconButton
    , { label = Text.foldEmpty
      , icon = Icon.foldEmpty
      , onFold = isEmpty
      }
        |> iconButton
    ]
        |> row [ spacing 3 ]


entryButton :
    { label : String
    , onPress : { delete : msg, duplicate : msg }
    , url : String
    }
    -> Element msg
entryButton { label, onPress, url } =
    [ Element.link [ width fill ]
        { url = url
        , label = el styleButton (text label)
        }
    , { label = defaultIcon Icon.duplicate
      , onPress = Just onPress.duplicate
      }
        |> Input.button styleButton
        |> el [ alignRight ]
    , { label = defaultIcon Icon.delete
      , onPress = Just onPress.delete
      }
        |> Input.button styleDeleteButton
        |> el [ alignRight ]
    ]
        |> row [ width fill, spacing 6 ]


formButton :
    { label : String
    , onPress : msg
    }
    -> Element msg
formButton { label, onPress } =
    Input.button styleButton
        { label = text label, onPress = Just onPress }


createForm :
    { cancel : msg
    , form : String
    , onChange : String -> msg
    , onCreate : Maybe msg
    , new : String
    }
    -> Element msg
createForm ({ cancel, onChange, onCreate, new } as datas) =
    regularEntryButton
        { action = regularNameButton { onChange = onChange, new = new }
        , cancel = cancel
        , confirm =
            Maybe.map
                (\msg ->
                    { icon = Icon.add
                    , label = Text.createAction
                    , onPress = msg
                    }
                        |> regularLabel styleConfirmButton
                )
                onCreate
        , label = text datas.form |> el styleStaticButton
        }


deleteEntry :
    { cancel : msg
    , entry : { r | label : String, url : String }
    , onDelete : msg
    }
    -> Element msg
deleteEntry { cancel, entry, onDelete } =
    regularEntryButton
        { action = text Text.deleteEntry |> el styleStaticButton
        , cancel = cancel
        , confirm =
            { icon = Icon.delete
            , label = Text.deleteEntryAction
            , onPress = onDelete
            }
                |> regularLabel styleDeleteButton
                |> Just
        , label =
            Element.link [ width fill ]
                { label = el styleButton (text entry.label)
                , url = entry.url
                }
        }


duplicateEntry :
    { cancel : msg
    , entry : { r | label : String, url : String }
    , onChange : String -> msg
    , onDuplicate : Maybe msg
    , new : String
    }
    -> Element msg
duplicateEntry { cancel, entry, onChange, onDuplicate, new } =
    regularEntryButton
        { action = regularNameButton { onChange = onChange, new = new }
        , cancel = cancel
        , confirm =
            Maybe.map
                (\msg ->
                    { icon = Icon.duplicate
                    , label = Text.duplicateAction
                    , onPress = msg
                    }
                        |> regularLabel styleShrinkButton
                )
                onDuplicate
        , label =
            Element.link [ width fill ]
                { label = el styleButton (text entry.label)
                , url = entry.url
                }
        }


createEntryButton :
    { cancel : msg
    , confirm : Maybe msg
    }
    -> Element msg
createEntryButton { cancel, confirm } =
    [ confirm
        |> Maybe.map
            (\confirmMsg ->
                 Input.button styleShrinkButton
                     { label =
                           [ defaultIcon Icon.add
                           , text Text.createAction
                           ]
                               |> row [ spacing 6 ]
                     , onPress = Just confirmMsg
                     }
            )
        |> Maybe.withDefault none
    , Input.button styleShrinkButton
        { label =
              [ defaultIcon Icon.cancel
              , text Text.cancelAction
              ]
                  |> row [ spacing 6 ]
        , onPress = Just cancel
        }
    ]
        |> row [ width shrink, spacing 6 ]

regularEntryButton :
    { action : Element msg
    , cancel : msg
    , confirm : Maybe (Element msg)
    , label : Element msg
    }
    -> Element msg
regularEntryButton { action, cancel, confirm, label } =
    [ label
    , [ action
      , Maybe.withDefault none confirm
      , Input.button styleShrinkButton
            { label =
                [ defaultIcon Icon.cancel
                , text Text.cancelAction
                ]
                    |> row [ spacing 6 ]
            , onPress = Just cancel
            }
      ]
        |> row [ width fill, spacing 6 ]
    ]
        |> column [ width fill, spacing 6, paddingXY 0 6 ]


regularNameButton : { onChange : String -> msg, new : String } -> Element msg
regularNameButton { onChange, new } =
    { label = Input.labelHidden "name entry"
    , onChange = onChange
    , placeholder = Nothing
    , text = new
    }
        |> Input.text styleFieldButton


regularLabel :
    List (Attribute msg)
    -> { icon : TypedSvg.Core.Svg msg, label : String, onPress : msg }
    -> Element msg
regularLabel attributes { icon, label, onPress } =
    { label =
        [ defaultIcon icon
        , text label
        ]
            |> row [ spacing 6 ]
    , onPress = Just onPress
    }
        |> Input.button attributes


foldDescription : Bool -> Element msg -> Element msg
foldDescription isFolded description =
    row []
        [ smallIcon (Icon.fold isFolded)
        , el [ paddingXY 9 0 ] description
        ]


defaultIcon : TypedSvg.Core.Svg msg -> Element msg
defaultIcon =
    html >> el styleDefaultIcon


smallIcon : TypedSvg.Core.Svg msg -> Element msg
smallIcon =
    html >> el styleSmallIcon



-- STYLES


styleLayout : List (Attribute msg)
styleLayout =
    [ width fill
    , height fill
    , clip
    , Font.color (element color.text.normal)
    , Background.color (element color.back.main)
    ]


styleNavBar : List (Attribute msg)
styleNavBar =
    [ width fill
    , spaceEvenly
    , paddingXY 12 6
    , Background.color (element color.back.navBar)
    ]


styleContent : List (Attribute msg)
styleContent =
    [ width fill
    , height fill
    , scrollbarY
    , paddingEach { bottom = 0, left = 0, right = 0, top = 12 }
    ]


styleFooter : List (Attribute msg)
styleFooter =
    [ width fill
    , padding 3
    , Font.color (element color.text.hint)
    , Font.size 16
    ]


styleForm : List (Attribute msg)
styleForm =
    [ width fill, height shrink, spacing 12 ]


styleHint : List (Attribute msg)
styleHint =
    [ width fill, height fill, padding 12, Font.color (element color.text.hint) ]


styleWarning : List (Attribute msg)
styleWarning =
    [ width fill
    , height fill
    , padding 12
    , Font.color (element color.input.hover.delete)
    ]


styleOneOfChoice : List (Attribute msg)
styleOneOfChoice =
    [ width fill
    , Border.widthXY 0 2
    , Border.color (element color.back.main)
    , Element.mouseOver [ Border.color (element color.input.button) ]
    ]


styleSomeOfChoice : List (Attribute msg)
styleSomeOfChoice =
    styleOneOfChoice


styleListItem : List (Attribute msg)
styleListItem =
    [ width fill
    , paddingEach { right = 0, top = 12, left = 12, bottom = 12 }
    , Border.widthEach
        { left = 3, top = 0, right = 0, bottom = 0 }
    , Border.color (element color.input.button)
    ]


styleFieldTitle : List (Attribute msg)
styleFieldTitle =
    [ width fill
    , Background.color (element color.back.navBar)
    , Border.rounded 6
    , padding 12
    , Element.focused []
    ]


styleControlButton : List (Attribute msg)
styleControlButton =
    [ padding 3
    , Background.color (element color.input.button)
    , Border.width 0
    , Border.rounded 6
    , Element.mouseOver [ Background.color (element color.input.hover.button) ]
    , Element.focused [ Border.color (element color.input.button) ]
    ]


styleInteractive : List (Attribute msg)
styleInteractive =
    [ width fill
    , padding 12
    , Border.rounded 6
    , Font.color (element color.text.normal)
    ]


styleSearchHelper : List (Attribute msg)
styleSearchHelper =
    styleInteractive
        ++ [ Input.focusedOnLoad
           , htmlAttribute (Html.Attributes.id focused)
           , Background.color (element color.back.main)
           , Border.color (element color.input.hover.button)
           , padding 9
           , Element.mouseOver
                [ Background.color (element color.input.text)
                , Border.color (element color.input.hover.text)
                ]
           , Element.focused
                [ Background.color (element color.input.text)
                , Border.color (element color.input.hover.text)
                ]
           ]


styleSearchInput : List (Attribute msg)
styleSearchInput =
    styleSearchHelper
        ++ [ width fill
           , Border.widthEach defaultWidth
           , paddingEach { defaultPadding | left = 40, right = 40 }
           , defaultIcon Icon.search
                |> el [ moveRight 32, moveDown 2, centerY ]
                |> onLeft
           ]


styleFieldButton : List (Attribute msg)
styleFieldButton =
    styleSearchHelper ++ [ Border.width 3, htmlAttribute (Html.Attributes.autocomplete False)]


styleButton : List (Attribute msg)
styleButton =
    styleInteractive
        ++ [ Border.width 0
           , Background.color (element color.input.button)
           , Element.mouseOver
                [ Background.color (element color.input.hover.button) ]
           , Element.focused [ Border.color (element color.input.button) ]
           ]


styleShrinkButton : List (Attribute msg)
styleShrinkButton =
    styleInteractive
        ++ [ width shrink
           , Border.width 0
           , Border.rounded 6
           , Background.color (element color.input.button)
           , Element.mouseOver
                [ Background.color (element color.input.hover.button) ]
           , Element.focused [ Border.color (element color.input.button) ]
           ]


styleStaticButton : List (Attribute msg)
styleStaticButton =
    styleInteractive ++ [ Background.color (element color.input.button) ]


styleConfirmButton : List (Attribute msg)
styleConfirmButton =
    styleInteractive
        ++ [ width shrink
           , alignRight
           , Border.width 0
           , Background.color (element color.input.confirm)
           , Element.mouseOver
                [ Background.color (element color.input.hover.confirm) ]
           , Element.focused [ Border.color (element color.input.confirm) ]
           ]


styleDeleteButton : List (Attribute msg)
styleDeleteButton =
    styleInteractive
        ++ [ width shrink
           , Border.width 0
           , Background.color (element color.input.delete)
           , Element.mouseOver
                [ Background.color (element color.input.hover.delete) ]
           , Element.focused [ Border.color (element color.input.delete) ]
           ]


styleHiddenButton : List (Attribute msg)
styleHiddenButton =
    [ width fill
    , Border.width 0
    , Element.focused [ Border.color (element color.back.main) ]
    ]


styleTooltip : List (Attribute msg)
styleTooltip =
    [ moveDown 3
    , Border.rounded 6
    , padding 12
    , spacing 3
    , centerY
    , width shrink
    , Background.color (element color.back.popup)
    ]


styleSelectItem : List (Attribute msg)
styleSelectItem =
    styleDatePickerHeader
        ++ [ width fill
           , padding 6
           , Font.alignLeft
           , Border.rounded 3
           ]


styleDatePickerHeader : List (Attribute msg)
styleDatePickerHeader =
    [ padding 6
    , Font.center
    , Border.rounded 6
    , Border.width 0
    , Background.color (element color.back.alpha)
    , Element.mouseOver [ Background.color (element color.input.text) ]
    , Element.focused [ Border.color (element color.input.text) ]
    ]


styleDatePicker : DatePicker.Settings
styleDatePicker =
    { firstDayOfWeek = Time.Mon
    , language = Just Text.calendar
    , disabled = always False
    , pickerAttributes = styleTooltip
    , headerAttributes = [ width fill, Font.bold ]
    , tableAttributes = [ height fill, centerY ]
    , weekdayAttributes = [ Font.center, Font.bold ]
    , dayAttributes = styleDatePickerHeader
    , monthYearAttribute =
        [ Border.rounded 6
        , padding 12
        , width fill
        , Font.center
        , centerY
        , mouseOver [ Background.color (element color.input.text) ]
        ]
    , wrongMonthDayAttributes = [ Font.color (element color.text.hint) ]
    , todayDayAttributes = []
    , selectedDayAttributes = [ Background.color (element color.input.button) ]
    , disabledDayAttributes =
        [ Font.color (element color.text.hint)
        , Background.color (element color.back.navBar)
        ]
    , monthsTableAttributes = [ spaceEvenly, width fill, height fill ]
    , yearsTableAttributes = [ spaceEvenly, width fill, height fill ]
    , headerButtonsAttributes = width fill :: styleDatePickerHeader
    , previousMonthElement = Element.text "←"
    , nextMonthElement = Element.text "→"
    }


styleSmallIcon : List (Attribute msg)
styleSmallIcon =
    [ width (px 12), height (px 12), centerX, centerY, moveUp 2 ]


styleDefaultIcon : List (Attribute msg)
styleDefaultIcon =
    [ width (px 20), height (px 20), centerX, centerY ]



-- WRAPPERS


defaultPadding : { bottom : Int, left : Int, right : Int, top : Int }
defaultPadding =
    { bottom = 9, left = 9, right = 9, top = 9 }


defaultWidth : { bottom : Int, left : Int, right : Int, top : Int }
defaultWidth =
    { bottom = 3, left = 3, right = 3, top = 3 }


rowGroup : List (Element msg) -> Element msg
rowGroup =
    row [ spacing 3 ]


map : (msg -> msg1) -> Element msg -> Element msg1
map =
    Element.map


none : Element msg
none =
    Element.none


layout : Element msg -> Html msg
layout =
    Element.layout styleLayout


link : { label : String, url : String } -> Element msg
link { label, url } =
    Element.link [] { label = text label, url = url }


newTabLink : { label : String, url : String } -> Element msg
newTabLink { label, url } =
    Element.newTabLink [] { label = text label, url = url }



-- HELPERS


wrappingText : String -> Element msg
wrappingText string =
    Element.paragraph [ width fill ]
        [ text string ]


info : String -> Element msg
info =
    text


todo : String -> Element msg
todo =
    text
        >> el
            [ padding 50
            , Font.color (element color.input.delete)
            ]


focused : String
focused =
    "focused"


onEnter : msg -> Element.Attribute msg
onEnter msg =
    let
        result : String -> Json.Decode.Decoder msg
        result key =
            if key == "Enter" then
                Json.Decode.succeed msg

            else
                Json.Decode.fail "no enter key event"
    in
    Json.Decode.field "key" Json.Decode.string
        |> Json.Decode.andThen result
        |> Html.Events.on "keyup"
        |> htmlAttribute


fillCenterPortionY : Int -> Element msg -> Element msg
fillCenterPortionY p elem =
    column [ width fill, height fill ]
        [ el [ width fill, height (fillPortion 1) ] none
        , el [ width fill, height (fillPortion p) ] elem
        , el [ width fill, height (fillPortion 1) ] none
        ]


fillCenterPortionX : Int -> Element msg -> Element msg
fillCenterPortionX p elem =
    row [ width fill, height fill ]
        [ el [ width (fillPortion 1), height fill ] none
        , el [ width (fillPortion p), height fill ] elem
        , el [ width (fillPortion 1), height fill ] none
        ]
