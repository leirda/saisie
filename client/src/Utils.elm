module Utils exposing (..)

import Date exposing (..)
import Time exposing (..)
import Time.Extra exposing (..)


-- HTTP


--expectFrom : () -> -> -> Cmd msg


-- TYPES


maybeIsNothing : Maybe a -> Bool 
maybeIsNothing =
    Maybe.map (\_ -> False)
        >> Maybe.withDefault True


-- TIME


dateToPosix : Date -> Posix
dateToPosix date =
    partsToPosix utc
        { year = year date
        , month = month date
        , day = day date
        , hour = 0
        , minute = 0
        , second = 0
        , millisecond = 0
        }


intFromMonth : Time.Month -> Int
intFromMonth month =
    case month of
        Time.Jan ->
            1

        Time.Feb ->
            2

        Time.Mar ->
            3

        Time.Apr ->
            4

        Time.May ->
            5

        Time.Jun ->
            6

        Time.Jul ->
            7

        Time.Aug ->
            8

        Time.Sep ->
            9

        Time.Oct ->
            10

        Time.Nov ->
            11

        Time.Dec ->
            12
