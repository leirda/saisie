module Form exposing
    ( Content(..)
    , Field
    , Form
    , FormFill
    , decodeContent
    , decodeEntry
    , decodeForm
    , encodeForm
    , foldButtons
    , formToString
    , view
    )

import Date exposing (Date)
import DatePicker
import Iso8601 as Date
import Json.Decode as JD
import Json.Decode.Extra as JD
import Json.Encode as JE
import Json.Encode.Extra as JE
import List.Extra as List
import Set exposing (Set)
import Text
import Time
import Ui exposing (Ui, field)
import Utils exposing (dateToPosix, intFromMonth, maybeIsNothing)



-- MODEL


{-| Ce type représente un dossier (ou une fiche) avec son nom, sa version et le
contenu.
-}
type alias Form =
    { fields : List Field
    , name : String
    , version : String
    }


{-| Ce type représente un champ utilisable une fois toutes les informations
récupérées.

Le `content` est le contenu saisie par l’utilisatrice.

La `description` est un texte supplémentaire que l’on affiche en dessous du nom
du champ.

`isFolded` est l’état déroulé (visible) ou non (on ne voit que le nom).

`name` est le nom du champ.

`isRequired` définit si le champ est requis et permet d’afficher un warning s’il
n’est pas renseigné.

-}
type alias Field =
    { content : Content
    , description : Maybe String
    , default : () -> Content
    , isDuplicable : Bool
    , isFolded : Bool
    , isRequired : Bool
    , name : String
    }


{-| Représente les différents types de champs que l’on peut avoir dans
l’interface. Par exemple, on a un constructeur `Line` qui permet d’afficher une
ligne de texte, ou encore un constructeur `SomeOf` pour afficher plusieurs choix
sélectionnables.
-}
type Content
    = FieldGroup (ContentDatas (List Field) (List Field))
    | FieldList
        { description : Maybe String
        , items : { isFolded : Bool, fields : List Field }
        , max : Int
        }
        (ContentDatas
            (List { isFolded : Bool, fields : List Field })
            (List (List Field))
        )
    | Line (ContentDatas String String)
    | Paragraph (ContentDatas String String)
    | YesOrNo (ContentDatas Bool Bool)
    | SomeOf
        (Set String)
        (ContentDatas
            { other : Maybe String
            , selected : Set String
            }
            (Set String)
        )
    | StrictSomeOf (Set String) (ContentDatas (Set String) (Set String))
    | OneOf (Set String) (ContentDatas (Maybe String) (Maybe String))
    | StrictOneOf (Set String) (ContentDatas String String)
    | SelectOneOf
        (Set String)
        (ContentDatas
            { input : String, bestMatches : List String, keepMatches : Bool }
            String
        )
    | StrictSelectOneOf
        (Set String)
        (ContentDatas
            { input : String, bestMatches : List String, keepMatches : Bool }
            String
        )
    | IntBetween { max : Int, min : Int } (ContentDatas String Int)
    | FloatBetween { max : Float, min : Float } (ContentDatas String Float)
    | Date (ContentDatas ( DatePicker.Model, String ) Int)
    | Year (ContentDatas String { year : Int, hint : String })


{-| Ce type représente tout ce qui est commun parmi tous les
[`Content`](Form#Content).

`content` représente le contenu au format saisi par l’utilisatrice.

`result` représente le contenu au format attendu une fois que le `content` a été
vérifié.

-}
type alias ContentDatas content result =
    { content : content
    , isEmpty : content -> Bool
    , parse : content -> Result String result
    }


{-| Représente les données saisies pour un dossier mais qui n’ont pas encore été
associées au `Form` correspondant.
-}
type alias FormFill =
    { fields : List FieldFill
    , name : String
    , version : String
    }


{-| Représente un contenu `fill` associée à un champ du nom `name` qui n’a pas
encore été associé au `Field` correspondant.
-}
type alias FieldFill =
    { name : String
    , fill : JD.Value
    }



-- VIEW


view : { r | year : Int } -> Form -> Ui Form
view datas ({ fields } as form) =
    List.indexedMap
        (\index field ->
            Ui.map
                (\new -> { form | fields = List.setAt index new fields })
                (viewField datas field)
        )
        fields
        |> Ui.form


viewField : { r | year : Int } -> Field -> Ui Field
viewField ({ year } as datas) ({ isFolded } as field) =
    let
        fieldInput :
            { contentDatas : ContentDatas content result
            , input :
                { content : content
                , onChange : content -> Content
                }
                -> Ui Content
            , makeField : ContentDatas content result -> Content
            }
            -> Ui Field
        fieldInput { contentDatas, input, makeField } =
            { content = contentDatas.content
            , datas = field
            , input =
                { content = contentDatas.content
                , onChange =
                    \newContent ->
                        makeField { contentDatas | content = newContent }
                }
                    |> input
                    |> Ui.map (\content -> { field | content = content })
            , onFold = { field | isFolded = not isFolded }
            }
                |> Ui.field
                    { isEmpty = contentDatas.isEmpty
                    , parse = contentDatas.parse
                    }
    in
    case field.content of
        FieldGroup contentDatas ->
            fieldInput
                { contentDatas = contentDatas
                , input = Ui.group (viewField datas)
                , makeField = FieldGroup
                }

        FieldList ({ description } as attributes) contentDatas ->
            let
                keepDuplicableFields : List Field -> List Field
                keepDuplicableFields =
                    (\thisField ->
                         if thisField.isDuplicable then
                             thisField
                         else
                             { thisField | content = thisField.default () }
                    )
                      |> List.map
            in
            fieldInput
                { contentDatas = contentDatas
                , input =
                    { add = attributes
                    , duplicate =
                        \item ->
                            { item
                                | fields =
                                  keepDuplicableFields item.fields
                            }
                    , view =
                        Ui.listItem
                            { description = description
                            , view = viewField datas
                            }
                    }
                        |> Ui.list
                , makeField = FieldList attributes
                }

        Line contentDatas ->
            fieldInput
                { contentDatas = contentDatas
                , input = Ui.textInput { hint = Nothing }
                , makeField = Line
                }

        Paragraph contentDatas ->
            fieldInput
                { contentDatas = contentDatas
                , input = Ui.paragraph
                , makeField = Paragraph
                }

        YesOrNo contentDatas ->
            fieldInput
                { contentDatas = contentDatas
                , input = Ui.bool
                , makeField = YesOrNo
                }

        SomeOf choices contentDatas ->
            fieldInput
                { contentDatas = contentDatas
                , input = Ui.someOf choices
                , makeField = SomeOf choices
                }

        StrictSomeOf choices contentDatas ->
            fieldInput
                { contentDatas = contentDatas
                , input = Ui.strictSomeOf choices
                , makeField = StrictSomeOf choices
                }

        OneOf choices contentDatas ->
            fieldInput
                { contentDatas = contentDatas
                , input = Ui.oneOf choices
                , makeField = OneOf choices
                }

        StrictOneOf choices contentDatas ->
            fieldInput
                { contentDatas = contentDatas
                , input = Ui.strictOneOf choices
                , makeField = StrictOneOf choices
                }

        SelectOneOf choices contentDatas ->
            fieldInput
                { contentDatas = contentDatas
                , input = Ui.select choices
                , makeField = SelectOneOf choices
                }

        StrictSelectOneOf choices contentDatas ->
            fieldInput
                { contentDatas = contentDatas
                , input = Ui.select choices
                , makeField = StrictSelectOneOf choices
                }

        Date contentDatas ->
            fieldInput
                { contentDatas = contentDatas
                , input = Ui.date
                , makeField = Date
                }

        Year ({ content, parse } as contentDatas) ->
            fieldInput
                { contentDatas = contentDatas
                , input =
                    Ui.textInput
                        { hint =
                            Result.toMaybe (parse content)
                                |> Maybe.map .hint
                        }
                , makeField = Year
                }

        IntBetween ({ min, max } as bounds) contentDatas ->
            fieldInput
                { contentDatas = contentDatas
                , input = Ui.textInput { hint = Nothing }
                , makeField = IntBetween bounds
                }

        _ ->
            Ui.todo "Je ne sais pas comment afficher cet item!"


foldButtons : { form : Form, onChange : Form -> msg } -> Ui msg
foldButtons { form, onChange } =
    { fields = form.fields
    , fold = \state field -> { field | isFolded = state }
    , isEmpty =
        \field ->
            case field.content of
                FieldGroup { content, isEmpty } ->
                    isEmpty content

                FieldList _ { content, isEmpty } ->
                    isEmpty content

                Line { content, isEmpty } ->
                    isEmpty content

                Paragraph { content, isEmpty } ->
                    isEmpty content

                YesOrNo { content, isEmpty } ->
                    isEmpty content

                SomeOf _ { content, isEmpty } ->
                    isEmpty content

                StrictSomeOf _ { content, isEmpty } ->
                    isEmpty content

                OneOf _ { content, isEmpty } ->
                    isEmpty content

                StrictOneOf _ { content, isEmpty } ->
                    isEmpty content

                SelectOneOf _ { content, isEmpty } ->
                    isEmpty content

                StrictSelectOneOf _ { content, isEmpty } ->
                    isEmpty content

                FloatBetween _ { content, isEmpty } ->
                    isEmpty content

                IntBetween _ { content, isEmpty } ->
                    isEmpty content

                Date { isEmpty, content } ->
                    isEmpty content

                Year { isEmpty, content } ->
                    isEmpty content
    , isFolded = .isFolded
    , onChange = (\new -> { form | fields = new }) >> onChange
    }
        |> Ui.foldButtons



-- CONTENT DECODERS


decodeEntry : JD.Decoder FormFill
decodeEntry =
    JD.map3 FormFill
        (JD.field "content" decodeContent)
        (JD.oneOf [ JD.field "form" JD.string, JD.field "name" JD.string ])
        (JD.field "version" (JD.oneOf [ JD.int |> JD.map String.fromInt, JD.string ]) |> JD.withDefault "")


decodeContent : JD.Decoder (List FieldFill)
decodeContent =
    JD.map
        (List.map (\( name, fill ) -> { fill = fill, name = name }))
        (JD.keyValuePairs JD.value)



-- FORM DECODERS


decodeForm : Date -> FormFill -> JD.Decoder Form
decodeForm date { fields, name, version } =
    JD.map2
        (\items formVersion ->
            { fields = items
            , name = name
            , version = formVersion
            }
        )
        (JD.field "items" (JD.list (fillField date fields)))
        (JD.field "version" (JD.oneOf [ JD.string, JD.int |> JD.map String.fromInt ]))


fillField : Date -> List FieldFill -> JD.Decoder Field
fillField date fields =
    JD.oneOf
        [ (\isDuplicable isRequired name description ->
            let
                ( matchedFillFields, _ ) =
                    List.partition (.name >> (==) name) fields
            in
            decodeType date
                { description = description
                , fill =
                    case matchedFillFields of
                        [ { fill } ] ->
                            Just fill

                        _ ->
                            Nothing
                , name = name
                , isRequired = isRequired
                , isDuplicable = isDuplicable
                }
                |> JD.field "type"
          )
            |> JD.succeed
            |> JD.andMap (JD.field "duplicable" JD.bool |> JD.withDefault True)
            |> JD.andMap (JD.field "required" JD.bool |> JD.withDefault False)
            |> JD.andMap (JD.field "name" JD.string)
            |> JD.andMap
                (JD.optionalNullableField "description" JD.string)
        , (\groupName description ->
            let
                ( matchedFillFields, _ ) =
                    List.partition (.name >> (==) groupName) fields

                makeFieldGroup : List Field -> Field
                makeFieldGroup content =
                    { content =
                        { content = content
                        , isEmpty = List.isEmpty
                        , parse = always (Ok content)
                        }
                            |> FieldGroup
                    , description = description
                    , default =
                        \_ ->
                            { content = content
                            , isEmpty = List.isEmpty
                            , parse = always (Ok content)
                            }
                                |> FieldGroup
                    , isDuplicable = True
                    , isFolded = True
                    , isRequired = False
                    , name = groupName
                    }
            in
            case matchedFillFields of
                { fill } :: _ ->
                    JD.field "items"
                        (resume decodeContent fill
                            |> JD.andThen
                                (\decodedFill ->
                                    JD.list (fillField date decodedFill)
                                )
                        )
                        |> JD.map makeFieldGroup

                _ ->
                    JD.field "items" (JD.list (fillField date []))
                        |> JD.map makeFieldGroup
          )
            |> JD.succeed
            |> JD.andMap (JD.field "group" JD.string)
            |> JD.andMap (JD.optionalNullableField "description" JD.string)
        ]
        |> JD.andThen identity


decodeType :
    Date
    ->
        { description : Maybe String
        , fill : Maybe JD.Value
        , name : String
        , isDuplicable : Bool
        , isRequired : Bool
        }
    -> JD.Decoder Field
decodeType date { description, fill, name, isDuplicable, isRequired } =
    let
        makeField :
            { decoder : JD.Decoder content
            , default : content
            , makeContent : content -> Content
            }
            -> JD.Decoder Field
        makeField { decoder, default, makeContent } =
            Maybe.map (resume (JD.oneOf [ decoder, JD.succeed default ])) fill
                |> Maybe.withDefault (JD.succeed default)
                |> JD.map
                    (\content ->
                        { content = makeContent content
                        , default = \_ -> makeContent default
                        , description = description
                        , isDuplicable = isDuplicable
                        , isFolded = True
                        , isRequired = isRequired
                        , name = name
                        }
                    )

        theString : String -> (JD.Decoder a -> JD.Decoder a)
        theString string =
            JD.when JD.string ((==) string)
    in
    [ theString "line" <|
        makeField
            { decoder = JD.string
            , default = ""
            , makeContent =
                \content ->
                    { content = content
                    , isEmpty = String.isEmpty
                    , parse = \_ -> Ok content
                    }
                        |> Line
            }
    , theString "paragraph" <|
        makeField
            { decoder = JD.string
            , default = ""
            , makeContent =
                \content ->
                    { content = content
                    , isEmpty = String.isEmpty
                    , parse = \_ -> Ok content
                    }
                        |> Paragraph
            }
    , theString "yesOrNo" <|
        makeField
            { decoder = JD.bool
            , default = False
            , makeContent =
                \content ->
                    { content = content
                    , isEmpty = \_ -> False
                    , parse = \_ -> Ok content
                    }
                        |> YesOrNo
            }
    , theString "date" <|
        makeField
            { decoder =
                JD.map
                    (Time.millisToPosix
                        >> Date.fromPosix Time.utc
                        >> (\selectedDate ->
                                ( DatePicker.initWithToday date
                                , Date.toIsoString selectedDate
                                )
                           )
                    )
                    JD.int
            , default =
                ( DatePicker.initWithToday date, "" )
            , makeContent =
                \content ->
                    { content = content
                    , isEmpty = String.isEmpty << Tuple.second
                    , parse =
                        Tuple.second
                            >> Date.fromIsoString
                            >> Result.map (dateToPosix >> Time.posixToMillis)
                    }
                        |> Date
            }
    , theString "duration" <|
        makeField
            { decoder = JD.map String.fromInt JD.int
            , default = ""
            , makeContent =
                \content ->
                    { content = content
                    , isEmpty = String.isEmpty
                    , parse =
                        String.toInt
                            >> Maybe.map
                                (\duration ->
                                    if duration < 0 then
                                        Err (Text.mustBeBetween ( 0, 1 / 0 ))

                                    else
                                        Ok duration
                                )
                            >> Maybe.withDefault (Err Text.mustBeAnInt)
                    }
                        |> IntBetween { max = round (1 / 0), min = 0 }
            }
    , theString "year" <|
        makeField
            { decoder = JD.map String.fromInt JD.int
            , default = ""
            , makeContent =
                \content ->
                    { content = content
                    , isEmpty = String.isEmpty
                    , parse =
                        String.toInt
                            >> Maybe.map
                                (\y ->
                                    if y < 1000 then
                                        { hint = Text.yearHint (Date.year date - y)
                                        , year = Date.year date - y
                                        }
                                            |> Ok

                                    else
                                        { hint = Text.yearIntervalHint (Date.year date - y)
                                        , year = y
                                        }
                                            |> Ok
                                )
                            >> Maybe.withDefault (Err Text.mustBeAnInt)
                    }
                        |> Year
            }
    , JD.field "intBetween"
        (JD.map2 (\min max -> { min = min, max = max })
            (JD.field "min" JD.int |> JD.withDefault (round (-1 / 0)))
            (JD.field "max" JD.int |> JD.withDefault (round (1 / 0)))
        )
        |> JD.andThen
            (\({ min, max } as bounds) ->
                { decoder = JD.map String.fromInt JD.int
                , default = ""
                , makeContent =
                    \content ->
                        { content = content
                        , isEmpty = String.isEmpty
                        , parse =
                            String.toInt
                                >> Maybe.map
                                    (\number ->
                                        if number < min || number > max then
                                            Err (Text.mustBeBetween ( toFloat min, toFloat max ))

                                        else
                                            Ok number
                                    )
                                >> Maybe.withDefault (Err Text.mustBeAnInt)
                        }
                            |> IntBetween bounds
                }
                    |> makeField
            )
    , JD.field "someOf" (JD.set JD.string)
        |> JD.andThen
            (\choices ->
                { decoder =
                    JD.map
                        (List.partition (\choice -> Set.member choice choices)
                            >> (\( selected, other ) ->
                                    { other = List.head other
                                    , selected = Set.fromList selected
                                    }
                               )
                        )
                        (JD.list JD.string)
                , default = { other = Nothing, selected = Set.empty }
                , makeContent =
                    \content ->
                        { content = content
                        , isEmpty =
                            \{ other, selected } ->
                                (maybeIsNothing other || other == Just "") && Set.isEmpty selected
                        , parse =
                            \{ other, selected } ->
                                Maybe.map (\c -> Set.insert c selected) other
                                    |> Maybe.withDefault selected
                                    |> Ok
                        }
                            |> SomeOf choices
                }
                    |> makeField
            )
    , JD.field "strictSomeOf" (JD.set JD.string)
        |> JD.andThen
            (\choices ->
                { decoder = JD.set JD.string
                , default = Set.empty
                , makeContent =
                    \content ->
                        { content = content
                        , isEmpty = Set.isEmpty
                        , parse = \_ -> Ok content
                        }
                            |> StrictSomeOf choices
                }
                    |> makeField
            )
    , JD.field "oneOf" (JD.set JD.string)
        |> JD.andThen
            (\choices ->
                { decoder = JD.map Just JD.string
                , default = Nothing
                , makeContent =
                    \content ->
                        { content = content
                        , isEmpty =
                            Maybe.map String.isEmpty >> Maybe.withDefault True
                        , parse = \_ -> Ok content
                        }
                            |> OneOf choices
                }
                    |> makeField
            )
    , JD.field "strictOneOf" (JD.set JD.string)
        |> JD.andThen
            (\choices ->
                { decoder = JD.string
                , default = ""
                , makeContent =
                    \content ->
                        { content = content
                        , isEmpty = String.isEmpty
                        , parse = \_ -> Ok content
                        }
                            |> StrictOneOf choices
                }
                    |> makeField
            )
    , JD.field "selectOneOf" (JD.set JD.string)
        |> JD.andThen
            (\choices ->
                { decoder = JD.string
                , default = ""
                , makeContent =
                    \content ->
                        { content =
                            { input = content
                            , bestMatches = []
                            , keepMatches = False
                            }
                        , isEmpty = .input >> String.isEmpty
                        , parse = .input >> Ok
                        }
                            |> SelectOneOf choices
                }
                    |> makeField
            )
    , JD.field "strictSelectOneOf" (JD.set JD.string)
        |> JD.andThen
            (\choices ->
                { decoder = JD.string
                , default = ""
                , makeContent =
                    \content ->
                        { content =
                            { input = content
                            , bestMatches = []
                            , keepMatches = False
                            }
                        , isEmpty = .input >> String.isEmpty
                        , parse =
                            \{ input } ->
                                if Set.member input choices then
                                    Ok input

                                else
                                    Err "Veuillez choisir un élément du menu déroulant."
                        }
                            |> StrictSelectOneOf choices
                }
                    |> makeField
            )
    , JD.field "list"
        (JD.map4
            (\fillItems desc items max ->
                { decoder =
                    decodeContent
                        |> JD.andThen
                            (fillField date
                                >> JD.list
                                >> (\decoder -> resume decoder fillItems)
                            )
                        |> JD.list
                , default = []
                , makeContent =
                    \content ->
                        FieldList
                            { description = desc
                            , items = items
                            , max = max
                            }
                            { content =
                                List.map
                                    (\toFold ->
                                        { isFolded = True
                                        , fields = toFold
                                        }
                                    )
                                    content
                            , isEmpty = List.isEmpty
                            , parse = \_ -> Ok content
                            }
                }
                    |> makeField
            )
            (JD.field "items" JD.value)
            (JD.oneOf
                [ JD.field "description" JD.string |> JD.map Just
                , JD.field "name" JD.string |> JD.map Just
                , JD.succeed Nothing
                ]
            )
            (JD.field "items"
                (JD.list (fillField date [])
                    |> JD.map
                        (\fields -> { isFolded = False, fields = fields })
                )
            )
            (JD.field "max" JD.int |> JD.withDefault (round (1 / 0)))
        )
        |> JD.andThen identity
    ]
        |> JD.oneOf


resume : JD.Decoder a -> JD.Value -> JD.Decoder a
resume decoder value =
    JD.decodeValue decoder value
        |> Result.mapError JD.errorToString
        |> JD.fromResult



-- FORM ENCODERS


encodeForm : Form -> JE.Value
encodeForm { fields, name, version } =
    [ ( "content", encodeFields fields )
    , ( "form", JE.string name )
    , ( "version", JE.string version )
    ]
        |> JE.object


formToString : Form -> String
formToString form =
    encodeForm form
        |> JE.encode 4


encodeFields : List Field -> JE.Value
encodeFields list =
    List.filterMap encodeContent list |> JE.object


encodeContent : Field -> Maybe ( String, JE.Value )
encodeContent ({ name } as field) =
    let
        encodeWithName : (a -> JE.Value) -> a -> ( String, JE.Value )
        encodeWithName encoder =
            Tuple.pair name >> Tuple.mapSecond encoder
    in
    case field.content of
        FieldGroup { isEmpty, content } ->
            encodeWithName encodeFields
                |> maybeUnless isEmpty content

        FieldList _ { isEmpty, content } ->
            encodeWithName (JE.list (.fields >> encodeFields))
                |> maybeUnless isEmpty content

        Line { isEmpty, content } ->
            encodeWithName JE.string |> maybeUnless isEmpty content

        Paragraph { isEmpty, content } ->
            encodeWithName JE.string |> maybeUnless isEmpty content

        YesOrNo { content, parse } ->
            case parse content of
                Err _ ->
                    Nothing

                Ok parsedContent ->
                    Just (encodeWithName JE.bool parsedContent)

        SomeOf _ { isEmpty, content, parse } ->
            encodeWithName
                (parse >> Result.withDefault Set.empty >> JE.set JE.string)
                |> maybeUnless isEmpty content

        StrictSomeOf _ { isEmpty, content } ->
            encodeWithName (JE.set JE.string)
                |> maybeUnless isEmpty content

        OneOf _ { isEmpty, content } ->
            encodeWithName (JE.string |> JE.maybe)
                |> maybeUnless isEmpty content

        StrictOneOf _ { isEmpty, content } ->
            encodeWithName JE.string |> maybeUnless isEmpty content

        SelectOneOf _ { isEmpty, content } ->
            encodeWithName (.input >> JE.string) |> maybeUnless isEmpty content

        StrictSelectOneOf _ { isEmpty, content, parse } ->
            case parse content of
                Err _ ->
                    Nothing

                Ok parsedContent ->
                    Just (encodeWithName JE.string parsedContent)

        IntBetween _ { content, parse } ->
            case parse content of
                Err _ ->
                    Nothing

                Ok parsedContent ->
                    Just (encodeWithName JE.int parsedContent)

        FloatBetween _ { content, parse } ->
            case parse content of
                Err _ ->
                    Nothing

                Ok parsedContent ->
                    Just (encodeWithName JE.float parsedContent)

        Date { content, parse } ->
            case parse content of
                Err _ ->
                    Nothing

                Ok parsedContent ->
                    Just (encodeWithName JE.int parsedContent)

        Year { content, parse } ->
            case parse content of
                Err _ ->
                    Nothing

                Ok parsedContent ->
                    Just (encodeWithName JE.int parsedContent.year)


maybeUnless : (a -> Bool) -> a -> (a -> b) -> Maybe b
maybeUnless pred a f =
    if pred a then
        Nothing

    else
        Just (f a)
