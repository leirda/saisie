module Main exposing (..)

import Browser
import Browser.Navigation as Nav
import Page.EditEntry as EditEntry
import Page.ListEntries as ListEntries
import Task
import Time
import Ui exposing (Ui)
import Url
import Url.Parser as Url exposing ((</>), s)
import Http exposing (Response(..))



-- MAIN


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        , subscriptions = subscriptions
        , update = update
        , view = view
        }


version : String
version =
    "v1.3.1"



-- MODEL


type alias Model =
    { datas :
        { key : Nav.Key
        , route : Route
        }
    , page : Page
    , year : Int
    }


type Page
    = BlankPage
    | PageEditEntry EditEntry.Model
    | PageListEntries ListEntries.Model


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    ( { datas = { key = key, route = parseRoute url }
      , page = BlankPage
      , year = 1970
      }
    , Task.map2 Time.toYear Time.here Time.now
        |> Task.perform GotYear
    )
        |> routePage



-- UPDATE


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | GotYear Int
    | MsgEditEntry EditEntry.Msg
    | MsgListEntries ListEntries.Msg
    | Noop


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ({ datas, page } as model) =
    case ( msg, page ) of
        ( LinkClicked urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl datas.key (Url.toString url) )

                Browser.External url ->
                    ( model, Nav.load url )

        ( GotYear year, _ ) ->
            ( { model | year = year }
            , Cmd.none
            )
                |> routePage

        ( UrlChanged url, _ ) ->
            ( { datas = { datas | route = parseRoute url }
              , page = page
              , year = model.year
              }
            , Cmd.none
            )
                |> routePage

        ( MsgEditEntry msgEditEntry, PageEditEntry pageEditEntry ) ->
            EditEntry.update datas pageEditEntry msgEditEntry
                |> mapUpdate PageEditEntry MsgEditEntry
                |> updatePage ( model, Cmd.none )

        ( MsgListEntries msgListEntries, PageListEntries pageListEntries ) ->
            ListEntries.update datas msgListEntries pageListEntries
                |> mapUpdate PageListEntries MsgListEntries
                |> updatePage ( model, Cmd.none )

        ( MsgListEntries _, BlankPage ) ->
            ListEntries.init model.year
                |> mapUpdate PageListEntries MsgListEntries
                |> updatePage ( model, Cmd.none )

        -- fallback to doing nothing for any other combinations
        ( _, _ ) ->
            ( model, Cmd.none )


updatePage : ( Model, Cmd msg ) -> ( Page, Cmd msg ) -> ( Model, Cmd msg )
updatePage ( model, modelCmd ) ( page, pageCmd ) =
    ( { model | page = page }
    , Cmd.batch [ modelCmd, pageCmd ]
    )


mapUpdate : (m -> n) -> (c -> d) -> ( m, Cmd c ) -> ( n, Cmd d )
mapUpdate mapModel mapMsg =
    Tuple.mapFirst mapModel >> Tuple.mapSecond (Cmd.map mapMsg)



-- ROUTES


type Route
    = EditEntry String
    | ListEntries
    | NotFound String


routeParser : Url.Parser (Route -> a) a
routeParser =
    Url.oneOf
        [ Url.map EditEntry (s "edit" </> Url.string)
        , Url.map ListEntries Url.top
        ]


parseRoute : Url.Url -> Route
parseRoute url =
    Url.parse routeParser url
        |> Maybe.withDefault (Url.toString url |> NotFound)


routePage : ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
routePage (( { datas, year }, _ ) as current) =
    case datas.route of
        EditEntry entry ->
            EditEntry.init entry
                |> mapUpdate PageEditEntry MsgEditEntry
                |> updatePage current

        ListEntries ->
            ListEntries.init year
                |> mapUpdate PageListEntries MsgListEntries
                |> updatePage current

        NotFound _ ->
            current



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions { page } =
    case page of
        PageEditEntry editEntry ->
            EditEntry.subscriptions editEntry
                |> Sub.map MsgEditEntry

        PageListEntries listEntries ->
            ListEntries.subscriptions listEntries
                |> Sub.map MsgListEntries

        BlankPage ->
            Sub.none



-- VIEW


view : Model -> Browser.Document Msg
view { page, year } =
    let
        pageContent : Ui Msg
        pageContent =
            case page of
                PageEditEntry content ->
                    EditEntry.view { year = year } content
                        |> Ui.map MsgEditEntry

                PageListEntries content ->
                    ListEntries.view content
                        |> Ui.map MsgListEntries

                BlankPage ->
                    Ui.none

        navBarContent : List (Ui Msg)
        navBarContent =
            case page of
                PageListEntries content ->
                    ListEntries.navBar content
                        |> List.map (Ui.map MsgListEntries)

                PageEditEntry content ->
                    EditEntry.navBar content
                        |> List.map (Ui.map MsgEditEntry)

                _ ->
                    []

        footerContent : Ui Msg
        footerContent =
            case page of
                PageEditEntry content ->
                    EditEntry.footer content
                        |> Ui.map MsgEditEntry

                _ ->
                    Ui.none
    in
    { title = "Saisie"
    , body =
        [ Ui.page
            { navBar = navBarContent
            , content = pageContent
            , footer =
                [ Ui.newTabLink
                    { url = "https://codeberg.org/leirda/saisie"
                    , label = "Saisie — " ++ version
                    }
                , footerContent
                , Ui.link
                    { url = "mailto:saisie@adac.asso.fr"
                    , label = "Support"
                    }
                ]
            }
            |> Ui.layout
        ]
    }
