module Page.ListEntries exposing
    ( Model
    , Msg(..)
    , init
    , navBar
    , subscriptions
    , update
    , view
    )

import Browser.Dom as Dom
import Browser.Navigation as Nav
import File exposing (File)
import File.Select as Select
import Http
import Json.Decode as JD exposing (list, string)
import Json.Encode as JE
import Task
import Text
import Ui exposing (Ui)
import Url.Builder exposing (absolute)



-- MODEL


type alias Model =
    { search : String
    , newName : Maybe String
    , page : Page
    , year : Int
    }


type Page
    = Entries
        { action : Action
        , entries : List String
        }
    | ErrorPage Http.Error
    | LoadingPage


type Action
    = Delete String
    | Duplicate { entry : String, new : String }
    | NoAction


init : Int -> ( Model, Cmd Msg )
init year =
    ( { search = ""
      , newName = Nothing
      , page = LoadingPage
      , year = year
      }
    , getEntries
    )



-- UPDATE


type Msg
    = GotError Http.Error
    | Loaded (List String)
    | Deleted (List String)
    | GotEntry { content : JD.Value, entry : String }
    | SavedEntry ()
    | Loading
    | Search String
    | DeleteRequest (Maybe String)
    | DeleteConfirm String
    | DuplicateRequest (Maybe { entry : String, new : String })
    | DuplicateConfirm { entry : String, new : String }
    | CreateEntryRequest (Maybe String)
    | CreateEntry String
    | EditEntry String
    | QuitToMain
    | ImportRequested
    | ImportSelected File
    | ImportReceived String String
    | Noop


update : { r | key : Nav.Key } -> Msg -> Model -> ( Model, Cmd Msg )
update { key } msg ({ page } as model) =
    case ( msg, page ) of
        ( GotError httpError, LoadingPage ) ->
            ( { model | page = ErrorPage httpError }
            , Cmd.none
            )

        ( Loaded entries, LoadingPage ) ->
            ( { model | page = Entries { action = NoAction, entries = entries } }
            , Task.attempt (always Noop) (Dom.focus Ui.focused)
            )

        ( Deleted entries, LoadingPage ) ->
            ( { model | page = Entries { action = NoAction, entries = entries } }
            , Task.attempt (always Noop) (Dom.focus Ui.focused)
            )

        ( Search search, Entries datas ) ->
            ( { model
                  | search =
                      Maybe.map (\_ -> model.search) model.newName
                          |> Maybe.withDefault search
                  , newName =
                      Maybe.map (\_ -> search) model.newName
              }
            , Cmd.none
            )

        ( QuitToMain, _ ) ->
            ( model, Nav.replaceUrl key (absolute [] []) )

        ( DeleteRequest entry, Entries datas ) ->
            ( { model
                | page =
                    Entries
                        { datas
                            | action =
                                Maybe.map Delete entry
                                    |> Maybe.withDefault NoAction
                        }
              }
            , Cmd.none
            )

        ( DeleteConfirm entry, Entries datas ) ->
            ( { model | page = LoadingPage }
            , deleteEntry entry
            )

        ( DuplicateRequest entry, Entries datas ) ->
            ( { model
                | page =
                    Entries
                        { datas
                            | action =
                                Maybe.map Duplicate entry
                                    |> Maybe.withDefault NoAction
                        }
              }
            , Cmd.none
            )

        ( DuplicateConfirm dup, Entries datas ) ->
            ( { model | page = LoadingPage }
            , getEntry dup
            )

        ( CreateEntryRequest entry, _ ) ->
            ( { model | newName = entry }
            , Cmd.none
            )

        ( CreateEntry entry, _ ) ->
            ( { model | page = LoadingPage }
            , createEntry entry
            )

        ( EditEntry entry, _ ) ->
            ( model, Nav.pushUrl key (absolute [ "edit", entry ] []) )

        ( GotEntry { content, entry }, LoadingPage ) ->
            ( { model | page = LoadingPage }
            , postEntry content entry
            )

        ( SavedEntry entry, LoadingPage ) ->
            init model.year

        ( ImportRequested, Entries _ ) ->
            ( model
            , Select.file [ "application/json" ] ImportSelected
            )

        ( ImportSelected file, _ ) ->
            ( model
            , File.toString file
                |> Task.perform
                   (File.name file
                       |> String.replace ".json" ""
                       |> ImportReceived
                   )
            )

        ( ImportReceived name json, Entries {entries} ) ->
            let
                decodedJson : Result JD.Error JD.Value
                decodedJson =
                    JD.decodeString JD.value json
            in
                case decodedJson of
                    Ok a -> 
                        ( { model | page = LoadingPage }
                        , postEntry a ( Text.makeUniqueName name entries )
                        )

                    Err _ -> ( model, Cmd.none )
                
        ( _, ErrorPage _ ) ->
            ( model, Cmd.none )

        ( Loading, _ ) ->
            ( { model | page = LoadingPage }
            , Cmd.none
            )

        ( _, _ ) ->
            ( model, Cmd.none )



-- HTTP


getEntries : Cmd Msg
getEntries =
    Http.get
        { expect = decodeToError (list string) Loaded
        , url = absolute [ "entries" ] []
        }


deleteEntry : String -> Cmd Msg
deleteEntry entry =
    Http.request
        { body = Http.emptyBody
        , expect = decodeToError (list string) Deleted
        , headers = []
        , method = "DELETE"
        , timeout = Nothing
        , tracker = Nothing
        , url = absolute [ "entry", entry ] []
        }


getEntry : { entry : String, new : String } -> Cmd Msg
getEntry { entry, new } =
    Http.get
        { expect =
            (\content -> GotEntry { content = content, entry = new })
                |> decodeToError JD.value
        , url = absolute [ "entry", entry ] []
        }


postEntry : JD.Value -> String -> Cmd Msg
postEntry content entry =
    Http.post
        { body = Http.jsonBody content
        , expect = decodeToError (JD.succeed ()) SavedEntry
        , url = absolute [ "entry", entry ] []
        }


createEntry : String -> Cmd Msg
createEntry new =
    Http.post
        { body =
            [ ( "form", JE.string "0000" )
            , ( "content", JE.object [] )
            ]
                |> JE.object
                |> Http.jsonBody
        , expect = Http.expectWhatever (\_ -> EditEntry new)
        , url = absolute [ "entry", new ] []
        }


decodeToError : JD.Decoder a -> (a -> Msg) -> Http.Expect Msg
decodeToError decoder msg =
    Http.expectJson
        (\result ->
            case result of
                Ok entries ->
                    msg entries

                Err httpError ->
                    GotError httpError
        )
        decoder



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions ({ page } as model) =
    case page of
        LoadingPage ->
            Sub.none

        _ ->
            Sub.none



-- VIEW


view : Model -> Ui Msg
view ({ page, year } as model) =
    let                    
        suggestName : String -> List String -> String
        suggestName form entries =
            [ form
            , "-"
            , String.fromInt year
            , "-"
            , List.filter (String.contains form) entries
                |> List.length
                |> (+) 1
                |> String.fromInt
                |> String.padLeft 3 '0'
            ]
                |> String.concat

        createButton entries =
            case model.newName of
                Nothing ->
                    if String.isEmpty model.search then
                        Ui.none

                    else
                        suggestName model.search entries
                            |> Just
                            |> CreateEntryRequest
                            |> Ui.createButton

                Just name ->
                    Ui.createEntryButton
                    { confirm =
                        if String.isEmpty name || List.member name entries then
                            Nothing

                        else
                            Just (CreateEntry name)
                    , cancel = CreateEntryRequest Nothing
                    }
    in
    case page of
        Entries ({ entries } as datas) ->
            { items =
                List.sort entries
                    |> List.map
                        (\entry ->
                             { button = viewButton datas entry, text = entry }
                        )
            , onChange = Search
            , text = Maybe.withDefault model.search model.newName
            , next = createButton entries
            }
                |> Ui.searchList

        _ ->
            { text = Maybe.withDefault model.search model.newName
            , onChange = Search
            , next = createButton []
            }
                |> Ui.search


viewButton :
    { action : Action, entries : List String }
    -> String
    -> Ui Msg
viewButton { action, entries } entry =
    let
        defaultEntry =
            { label = entry
            , url = absolute [ "edit", entry ] []
            , onPress =
                { delete = DeleteRequest (Just entry)
                , duplicate =
                    DuplicateRequest
                        (Just
                            { entry = entry
                            , new = entry ++ Text.duplicateEntry
                            }
                        )
                }
            }
    in
    case action of
        Duplicate dup ->
            if entry == dup.entry then
                { cancel = DuplicateRequest Nothing
                , entry = defaultEntry
                , new = dup.new
                , onChange = \new -> DuplicateRequest (Just { dup | new = new })
                , onDuplicate =
                    if String.isEmpty dup.new || List.member dup.new entries then
                        Nothing

                    else
                        Just (DuplicateConfirm dup)
                }
                    |> Ui.duplicateEntry

            else
                Ui.entryButton defaultEntry

        Delete this ->
            if entry == this then
                Ui.deleteEntry
                    { cancel = DeleteRequest Nothing
                    , entry = defaultEntry
                    , onDelete = DeleteConfirm this
                    }

            else
                Ui.entryButton defaultEntry

        NoAction ->
            Ui.entryButton defaultEntry


navBar : Model -> List (Ui Msg)
navBar model =
    Ui.quitButton { onPress = QuitToMain } :: List.singleton (Ui.importButton { onPress = ImportRequested } )
