module Page.EditEntry exposing
    ( Model
    , Msg(..)
    , footer
    , init
    , navBar
    , subscriptions
    , update
    , view
    )

import Browser.Navigation as Nav
import Browser.Events as Events
import Date exposing (Date)
import File.Download
import Form exposing (Field, Form)
import Http
import List.Extra as List
import Task
import Text
import Time
import Ui exposing (Ui)
import Url.Builder exposing (absolute)



-- MODEL


type alias Model =
    { entry : String
    , date : Date
    , state : State
    , page : Page
    }


type State
    = Modified
    | Saving
    | SavingAuto
    | Saved
    | ErrorState
    | InitialLoad


type Page
    = EditPage { form : Form }
    | ErrorPage Http.Error
    | LoadingPage


init : String -> ( Model, Cmd Msg )
init entry =
    ( { date = Date.fromCalendarDate 2020 Time.Jan 1
      , entry = entry
      , state = InitialLoad
      , page = LoadingPage
      }
    , Cmd.batch
        [ getEntry entry
        , Date.today |> Task.perform ReceiveDate
        ]
    )



-- UPDATE


type Msg
    = EntryLoaded Form.FormFill
    | FormChanged Form
    | FormLoaded Form
    | GetContent
    | GotError Http.Error
    | QuitToMain
    | NothingHappened
    | SaveAutoRequest
    | SaveRequest
    | SaveRequestAndQuitToMain
    | ReceiveDate Date
    | ExportForm


update : { r | key : Nav.Key } -> Model -> Msg -> ( Model, Cmd Msg )
update { key } ({ page } as model) msg =
    case ( page, msg ) of
        ( EditPage f, EntryLoaded content ) ->
            ( { model | state = Saved }
            , getForm model.date content
            )

        ( _, EntryLoaded content ) ->
            ( { model
                | page = LoadingPage
                , state =
                    if model.state == InitialLoad then
                        InitialLoad

                    else
                        Saved
              }
            , getForm model.date content
            )

        ( _, GetContent ) ->
            ( { model | page = LoadingPage }, getEntry model.entry )

        ( _, GotError error ) ->
            ( { model | page = ErrorPage error, state = ErrorState }, Cmd.none )

        ( EditPage { form }, FormLoaded loaded ) ->
            ( { model | page = EditPage { form = form } }
            , Cmd.none
            )

        ( _, FormLoaded form ) ->
            ( { model
                | page = EditPage { form = form }
              }
            , Cmd.none
            )

        ( _, ReceiveDate today ) ->
            ( { model | date = today }
            , Cmd.none
            )

        ( _, QuitToMain ) ->
            ( model, Nav.back key 1 )

        ( EditPage { form }, SaveAutoRequest ) ->
            if model.state == Saved || model.state == InitialLoad then
                ( model, Cmd.none )

            else
                ( { model | state = SavingAuto }
                , saveEntry EntryLoaded model.entry form
                )

        ( EditPage { form }, SaveRequest ) ->
            ( { model | page = LoadingPage, state = Saving }
            , saveEntry EntryLoaded model.entry form
            )

        ( EditPage { form }, SaveRequestAndQuitToMain ) ->
            ( { model | page = LoadingPage, state = Saving }
            , saveEntry (always QuitToMain) model.entry form
            )

        ( EditPage editPage, FormChanged newForm ) ->
            ( { model
                | page = EditPage { editPage | form = newForm }
                , state = Modified
              }
            , Cmd.none
            )

        ( EditPage { form }, ExportForm ) ->
            ( model
            , Form.formToString form
                |> File.Download.string (model.entry ++ ".json") "application/json"
            )


        -- Ignores everything else (e.g it doesn't matter to cover
        -- what would happen if a field was changed on the error
        -- page.) Beware this fallback case may catches things
        -- that you wouldn't want to miss !
        ( _, _ ) ->
            ( model, Cmd.none )


getEntry : String -> Cmd Msg
getEntry entry =
    Http.get
        { expect =
            Http.expectJson
                (\result ->
                    case result of
                        Ok content ->
                            EntryLoaded content

                        Err error ->
                            GotError error
                )
                Form.decodeEntry
        , url = absolute [ "entry", entry ] []
        }


getForm : Date -> Form.FormFill -> Cmd Msg
getForm date ({ name } as fill) =
    Http.get
        { expect =
            Http.expectJson
                (\result ->
                    case result of
                        Ok emptyForm ->
                            FormLoaded emptyForm

                        Err error ->
                            GotError error
                )
                (Form.decodeForm date fill)
        , url = absolute [ "form", name ] []
        }


saveEntry : (Form.FormFill -> Msg) -> String -> Form -> Cmd Msg
saveEntry msg entry form =
    Http.post
        { body = Http.jsonBody (Form.encodeForm form)
        , expect =
            Http.expectJson
                (\result ->
                    case result of
                        Ok content ->
                            msg content

                        Err error ->
                            GotError error
                )
                Form.decodeEntry
        , url = absolute [ "entry", entry ] []
        }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    [ Events.onVisibilityChange
          (\visibility ->
               case visibility of
                   Events.Hidden ->
                       SaveAutoRequest

                   Events.Visible ->
                       NothingHappened
          )
    , Time.every (5 * 60 * 1000) (\_ -> SaveAutoRequest)
    ]
          |> Sub.batch



-- VIEW


view : { r | year : Int } -> Model -> Ui Msg
view datas ({ page } as model) =
    case page of
        EditPage { form } ->
            Form.view datas form
                |> Ui.map FormChanged

        ErrorPage error ->
            case error of
                Http.BadBody body ->
                    Ui.error
                        { content = body
                        , control = [ Ui.retry GetContent ]
                        }

                Http.BadStatus status ->
                    Ui.error
                        { content = Text.error.badStatus status
                        , control = [ Ui.retry GetContent ]
                        }

                _ ->
                    Ui.error
                        { content = "TODO"
                        , control = [ Ui.retry GetContent ]
                        }

        LoadingPage ->
            Ui.none


navBar : { m | page : Page, state : State }-> List (Ui Msg)
navBar { page, state } =
    case page of
        EditPage { form } ->
            Ui.rowGroup
                [ Ui.quitButton { onPress = SaveRequestAndQuitToMain }
                , if state == Saved || state == InitialLoad then
                      Ui.none

                  else
                      Ui.applyButton { onPress = SaveRequest }
                ]
                :: Form.foldButtons
                    { form = form
                    , onChange = FormChanged
                    }
                :: Ui.none
                :: Ui.none
                :: [ Ui.exportButton { onPress = ExportForm } ]

        _ ->
            [ Ui.quitButton { onPress = QuitToMain } ]


footer : { m | state : State } -> Ui Msg
footer { state } =
    case state of
        ErrorState ->
            Ui.info Text.contentHaveError

        Modified ->
            Ui.info Text.contentIsModified

        Saving ->
            Ui.info Text.contentIsSaving

        SavingAuto ->
            Ui.info Text.contentIsSaving

        Saved ->
            Ui.info Text.contentSaved

        InitialLoad ->
            Ui.none
