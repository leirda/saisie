module Text exposing (..)

import Date exposing (Language)
import Time exposing (Month(..), Weekday(..))



search : String
search =
    "Rechercher…"


formsNumber : Int -> Int -> String
formsNumber found total =
    let
        zeppelin : String
        zeppelin =
            String.fromInt total ++ " au total"
    in
    if found == total then
        zeppelin

    else if found <= 1 then
        String.fromInt found ++ " trouvé | " ++ zeppelin

    else
        String.fromInt found ++ " trouvés | " ++ zeppelin


otherField : String
otherField =
    "Autre"


item : String
item =
    "Élément"


deleteEntry : String
deleteEntry =
    "Supprimer la fiche\u{202F}?"


makeUniqueName : String -> List String -> String
makeUniqueName entryName entries = 
    if List.member entryName entries then 
        makeUniqueName (entryName ++ duplicateEntry) entries
    else entryName


duplicateEntry : String
duplicateEntry =
    " - Copie"


createAction : String
createAction =
    "Créer"


duplicateAction : String
duplicateAction =
    "Dupliquer"


deleteEntryAction : String
deleteEntryAction =
    "Supprimer"


saveAndGoToMain : String
saveAndGoToMain =
    "Appliquer"


exportButtonText : String
exportButtonText =
    "Exporter"


importButtonText : String
importButtonText =
    "Importer"


dontSaveAndGoToMain : String
dontSaveAndGoToMain =
    "Abandonner"


quitToMain : String
quitToMain =
    "Terminer"


cancelAction : String
cancelAction =
    "Annuler"


contentIsModified : String
contentIsModified =
    "La fiche a été modifiée."


contentIsSaving : String
contentIsSaving =
    "Sauvegarde en cours…"


contentSaved : String
contentSaved =
    "Fiche sauvegardée\u{00A0}!"


contentHaveError : String
contentHaveError =
    "Une erreur s’est produite…"



-- FIELDS


datePlaceholder : String
datePlaceholder =
    "AAAA-MM-JJ"


yearHint : Int -> String
yearHint number =
    "(en " ++ String.fromInt number ++ ")"


yearIntervalHint : Int -> String
yearIntervalHint number =
    if number > 0 then
        case number of
            0 ->
                ""

            1 ->
                "(il y a 1 an)"

            x ->
                "(il y a " ++ String.fromInt x ++ " ans)"

    else
        case abs number of
            0 ->
                ""

            1 ->
                "(dans 1 an)"

            x ->
                "(dans " ++ String.fromInt x ++ " ans)"



-- ERRORS


http404 : String
http404 =
    "Ooops…! Cette page n’existe pas !"


error :
    { badStatus : Int -> String
    , generic : List String
    , retry : String
    , title : String
    }
error =
    { badStatus =
        \status ->
            "L’application a renvoyé le code d’erreur "
                ++ String.fromInt status
                ++ "."
    , generic =
        [ "Désolé, une erreur est survenue…"
        , "Vous pouvez copiez le texte suivant pour rapporter un bug."
        ]
    , title = "Erreur\u{202F}!"
    , retry = "Réessayer"
    }



-- FOLDS


foldAll : String
foldAll =
    "Tout enrouler"


unfoldAll : String
unfoldAll =
    "Tout dérouler"


foldEmpty : String
foldEmpty =
    "Enrouler les items vides"


unfoldEmpty : String
unfoldEmpty =
    "Dérouler les items vides"



-- FIELDS ERRORS AND CALENDAR


mustBeAnInt : String
mustBeAnInt =
    "Cet item doit être un nombre entier\u{00A0}!"


isRequired : String
isRequired =
    "Cet item est requis."


mustBeBetween : ( Float, Float ) -> String
mustBeBetween ( min, max ) =
    if min == -1 / 0 then
        "Ce nombre doit être inférieur à " ++ String.fromFloat max

    else if max == 1 / 0 then
        "Ce nombre doit être supérieur à " ++ String.fromFloat min

    else
        "Ce nombre doit être compris entre "
            ++ String.fromFloat min
            ++ " et "
            ++ String.fromFloat max


calendar : Language
calendar =
    { dayWithSuffix =
        String.fromInt
    , monthName =
        \month ->
            case month of
                Jan ->
                    "Janvier"

                Feb ->
                    "Février"

                Mar ->
                    "Mars"

                Apr ->
                    "Avril"

                May ->
                    "Mai"

                Jun ->
                    "Juin"

                Jul ->
                    "Juillet"

                Aug ->
                    "Août"

                Sep ->
                    "Septembre"

                Oct ->
                    "Octobre"

                Nov ->
                    "Novembre"

                Dec ->
                    "Décembre"
    , monthNameShort =
        \month ->
            case month of
                Jan ->
                    "Jan"

                Feb ->
                    "Fév"

                Mar ->
                    "Mars"

                Apr ->
                    "Avr"

                May ->
                    "Mai"

                Jun ->
                    "Juin"

                Jul ->
                    "Juil"

                Aug ->
                    "Août"

                Sep ->
                    "Sept"

                Oct ->
                    "Oct"

                Nov ->
                    "Nov"

                Dec ->
                    "Déc"
    , weekdayName =
        \weekday ->
            case weekday of
                Mon ->
                    "Lundi"

                Tue ->
                    "Mardi"

                Wed ->
                    "Mercredi"

                Thu ->
                    "Jeudi"

                Fri ->
                    "Vendredi"

                Sat ->
                    "Samedi"

                Sun ->
                    "Dimanche"
    , weekdayNameShort =
        \weekday ->
            case weekday of
                Mon ->
                    "lu"

                Tue ->
                    "ma"

                Wed ->
                    "me"

                Thu ->
                    "je"

                Fri ->
                    "ve"

                Sat ->
                    "sa"

                Sun ->
                    "di"
    }
