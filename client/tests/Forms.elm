module Forms exposing (..)

import Expect
import Form exposing (Content(..))
import Fuzz
import Json.Encode as JE
import String.Extra as String
import Test exposing (..)


suite : Test
suite =
    let
        formTemplate :
            { content : String, form : String, version : String }
            -> String
        formTemplate { content, form, version } =
            """{"content":{CONTENT},"form":{FORM},"version":{VERSION}}"""
                |> String.replace "{CONTENT}" content
                |> String.replace "{FORM}" form
                |> String.replace "{VERSION}" version

        emptyTemplate : { content : String, form : String, version : String }
        emptyTemplate =
            { content = "{}"
            , form = String.quote "test"
            , version = String.quote "v1"
            }
    in
    describe "The Form module"
        [ describe "Form.encodeForm"
            [ test "encodes empty forms" <|
                \_ ->
                    formTemplate emptyTemplate
                        |> Expect.equal
                            (Form.encodeForm
                                { fields = []
                                , name = "test"
                                , version = "v1"
                                }
                                |> JE.encode 0
                            )
            , fuzz Fuzz.string "doesn't change the form" <|
                \form ->
                    formTemplate
                        { emptyTemplate
                            | form = JE.string form |> JE.encode 0
                        }
                        |> Expect.equal
                            (Form.encodeForm
                                { fields = []
                                , name = form
                                , version = "v1"
                                }
                                |> JE.encode 0
                            )
            , fuzz Fuzz.string "doesn't change the version" <|
                \version ->
                    formTemplate
                        { emptyTemplate
                            | version = JE.string version |> JE.encode 0
                        }
                        |> Expect.equal
                            (Form.encodeForm
                                { fields = []
                                , name = "test"
                                , version = version
                                }
                                |> JE.encode 0
                            )
            , fuzz Fuzz.string "contains a single string content" <|
                \content ->
                    formTemplate
                        { emptyTemplate
                            | content =
                                JE.object
                                    [ ( "A single string content", JE.string content ) ]
                                    |> JE.encode 0
                        }
                        |> Expect.equal
                            (Form.encodeForm
                                { fields =
                                    [ { content =
                                            { content = content
                                            , isEmpty = always False
                                            , parse = always (Ok content)
                                            }
                                                |> Line
                                      , description = Nothing
                                      , default = Nothing
                                      , isFolded = False
                                      , name = "A single string content"
                                      , isRequired = False
                                      }
                                    ]
                                , name = "test"
                                , version = "v1"
                                }
                                |> JE.encode 0
                            )
            , fuzz Fuzz.string "contains a single string content but paragraph" <|
                \content ->
                    formTemplate
                        { emptyTemplate
                            | content =
                                JE.object
                                    [ ( "A single string content", JE.string content ) ]
                                    |> JE.encode 0
                        }
                        |> Expect.equal
                            (Form.encodeForm
                                { fields =
                                    [ { content =
                                            { content = content
                                            , isEmpty = always False
                                            , parse = always (Ok content)
                                            }
                                                |> Paragraph
                                      , description = Nothing
                                      , default = Nothing
                                      , isFolded = False
                                      , name = "A single string content"
                                      , isRequired = False
                                      }
                                    ]
                                , name = "test"
                                , version = "v1"
                                }
                                |> JE.encode 0
                            )
            , fuzz Fuzz.int "contains a single int content (IntBetween)" <|
                \content ->
                    formTemplate
                        { emptyTemplate
                            | content =
                                JE.object
                                    [ ( "A single int content", JE.int content ) ]
                                    |> JE.encode 0
                        }
                        |> Expect.equal
                            (Form.encodeForm
                                { fields =
                                    [ { content =
                                            { content = String.fromInt content
                                            , isEmpty = always False
                                            , parse = always (Ok content)
                                            }
                                                |> IntBetween {max = round(1/0), min = round(-1/0)}
                                      , description = Nothing
                                      , default = Nothing
                                      , isFolded = False
                                      , name = "A single int content"
                                      , isRequired = False
                                      }
                                    ]
                                , name = "test"
                                , version = "v1"
                                }
                                |> JE.encode 0
                            )
            , fuzz Fuzz.float "contains a single float content (FloatBetween)" <|
                \content ->
                    formTemplate
                        { emptyTemplate
                            | content =
                                JE.object
                                    [ ( "A single float content", JE.float content ) ]
                                    |> JE.encode 0
                        }
                        |> Expect.equal
                            (Form.encodeForm
                                { fields =
                                    [ { content =
                                            { content = String.fromFloat content
                                            , isEmpty = always False
                                            , parse = always (Ok content)
                                            }
                                                |> FloatBetween {max = 1/0, min = -1/0}
                                      , description = Nothing
                                      , default = Nothing
                                      , isFolded = False
                                      , name = "A single float content"
                                      , isRequired = False
                                      }
                                    ]
                                , name = "test"
                                , version = "v1"
                                }
                                |> JE.encode 0
                            )
            , fuzz Fuzz.int "contains a single int content (Year)" <|
                \content ->
                    formTemplate
                        { emptyTemplate
                            | content =
                                JE.object
                                    [ ( "A single int content", JE.int content ) ]
                                    |> JE.encode 0
                        }
                        |> Expect.equal
                            (Form.encodeForm
                                { fields =
                                    [ { content =
                                            { content = String.fromInt content
                                            , isEmpty = always False
                                            , parse = always (Ok ({hint = "dsdfjhsdhjlfhjsdfhjkfs", year = content}))
                                            }
                                                |> Year
                                      , description = Nothing
                                      , default = Nothing
                                      , isFolded = False
                                      , name = "A single int content"
                                      , isRequired = False
                                      }
                                    ]
                                , name = "test"
                                , version = "v1"
                                }
                                |> JE.encode 0
                            )
            ]
        ]
