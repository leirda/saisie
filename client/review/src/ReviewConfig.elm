-- SPDX-FileCopyrightText: 2022 Adriel Dumas--Jondeau
--
-- SPDX-License-Identifier: CC0-1.0


module ReviewConfig exposing (config)

{-| Do not rename the ReviewConfig module or the config function, because
`elm-review` will look for these.

To add packages that contain rules, add them to this review project using

    `elm install author/packagename`

when inside the directory containing this file.

-}

import NoBooleanCase
import NoInvalidRGBValues
import NoMissingTypeAnnotation
import NoMissingTypeAnnotationInLetIn
import NoMissingTypeExpose
import NoRedundantConcat
import NoSinglePatternCase
import NoUrlStringConcatenation
import NoUnused.Dependencies
import NoUnused.Exports
import NoUnused.Patterns
import Review.Rule exposing (Rule)


config : List Rule
config =
    [ NoBooleanCase.rule
    , NoInvalidRGBValues.rule
    , NoRedundantConcat.rule
    , NoUrlStringConcatenation.rule
    , NoSinglePatternCase.rule
        NoSinglePatternCase.fixInArgument
    , NoUnused.Dependencies.rule
    , NoUnused.Exports.rule
    , NoUnused.Patterns.rule
    , NoMissingTypeAnnotation.rule
    , NoMissingTypeAnnotationInLetIn.rule
    , NoMissingTypeExpose.rule
    ]
